package com.huataisi.oa.flow;

import org.activiti.engine.impl.interceptor.Session;
import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.engine.impl.persistence.entity.UserManager;

public class OAUserManagerFactory implements SessionFactory {

	@Override
	public Class<?> getSessionType() {
		return UserManager.class;  
	}

	@Override
	public Session openSession() {
		return new OAUserManager(); 
	}

}
