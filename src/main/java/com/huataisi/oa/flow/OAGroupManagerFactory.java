package com.huataisi.oa.flow;

import org.activiti.engine.impl.interceptor.Session;
import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.engine.impl.persistence.entity.GroupManager;

public class OAGroupManagerFactory implements SessionFactory {

	 @Override  
     public Class<?> getSessionType() {  
         return GroupManager.class;  
     }  
   
     @Override  
     public Session openSession() {  
         return new OAGroupManager();  
     }  

}
