package com.huataisi.oa.leave;

import com.huataisi.oa.domain.Duty;
import com.huataisi.oa.domain.User;
import com.huataisi.oa.security.UserUtil;
import com.huataisi.oa.util.AutoEntityView;
import com.huataisi.oa.util.ContainerUtils;
import com.huataisi.oa.web.EntityEditor;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.ui.Table;

@SuppressWarnings("serial")
public class LeaveView extends AutoEntityView<Leave> {

    public LeaveView() {
		super(Leave.class);
	}

	@Override
    protected EntityEditor createForm() {
        return new LeaveForm();
    }

    @Override
    protected void configureTable(Table table) {
        setFilter();
        table.setVisibleColumns(new String[]{"createBy","leaveType","startDate","endDate","countHours","backDate", "flowStatus","createDate"});
    }

    /**
     * 设置数据权限
     */
    private void setFilter(){
    	User user = UserUtil.getCurrentUser();
    	JPAContainer<Leave> con = getTableContainer();
    	con.removeAllContainerFilters();
    	if(Duty.ZHI_YUAN.equals(user.getDuty().getName())){
    		//职员只能看自己的
    		Filter filter = new Compare.Equal("createBy", user);
    		con.addContainerFilter(filter);
    	}else if(Duty.BU_MENG_JING_LI.equals(user.getDuty().getName())
    			&& "后勤部".equals(user.getDepartment().getName())
    			){
    		//后勤部门经理可以看全部
	    }else if(Duty.BU_MENG_JING_LI.equals(user.getDuty().getName())){
	    	//部门经理只能查看本部门的
	    	Filter filter = new Compare.Equal("createBy.department", user.getDepartment());
	    	con.addContainerFilter(filter);
	    }
    	
    }
    
}
