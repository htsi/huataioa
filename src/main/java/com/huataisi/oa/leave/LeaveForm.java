package com.huataisi.oa.leave;

import java.util.Date;
import java.util.Vector;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.util.CollectionUtil;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.huataisi.oa.domain.User;
import com.huataisi.oa.flow.ProcessDefinitionImageStreamResourceBuilder;
import com.huataisi.oa.security.UserUtil;
import com.huataisi.oa.util.EntityFieldFactory;
import com.huataisi.oa.web.AutomaticEntityForm;
import com.vaadin.data.Item;
import com.vaadin.terminal.StreamResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Embedded;

@SuppressWarnings("serial")
@Configurable
public class LeaveForm extends AutomaticEntityForm<com.huataisi.oa.leave.Leave> {

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private RepositoryService repositoryService;

	public LeaveForm() {
		super(com.huataisi.oa.leave.Leave.class);
		getForm().setFormFieldFactory(new EntityFieldFactory());
		getForm().setWidth("260px");
	}

	@Override
	public void setItemDataSource(Item item) {
		super.setItemDataSource(item);
		Vector<String> v = new Vector<String>();
		v.add("leaveType");
		v.add("startDate");
		v.add("endDate");
		v.add("countHours");
		v.add("reason");
		getForm().setVisibleItemProperties(v);
		if(item != null){
			getForm().getField("leaveType").setCaption("请假类型");
			getForm().getField("startDate").setCaption("开始时间");
			getForm().getField("endDate").setCaption("结束时间");
			getForm().getField("countHours").setCaption("合计天数");
			getForm().getField("reason").setCaption("请假原因");

			//设置只读编辑状态
			User loginUser = UserUtil.getCurrentUser();
			User leaveUser = (User) item.getItemProperty("createBy").getValue();
			Object status = item.getItemProperty("flowStatus").getValue();
			if((leaveUser != null && loginUser.getId() != leaveUser.getId()) //别人的
					|| (leaveUser != null && !"退回修改".equals(status) &&  loginUser.getId() == leaveUser.getId()) //自己的但是正审批中
					){
				//设置为只读
				setSaveAllowed(false);
				setDeleteAllowed(false);

				//增加工作流按钮
				addWorkFlowButtons(item);

			}else{
				//可编辑
				setSaveAllowed(true);
				setDeleteAllowed(true);
			}

			//添加流程图
			addFolwChat(item);
		}
	}


	private void addWorkFlowButtons(final Item item) {
		this.clearFlowActions();
		User loginUser = UserUtil.getCurrentUser();
		final Task task = taskService.createTaskQuery()
				.processInstanceBusinessKey(getBSKey(item))
				.taskAssignee(loginUser.getUsername())
				.singleResult();

		
		if(task == null){
			return;
		}
		String caption = "通过";
		if("填写申请".equals(task.getName())){
			caption = "请领导审批";
		}else if("放假中".equals(task.getName())){
			caption = "销假";
		}
		Button agreebt = new Button(caption);
		this.addFlowAction(agreebt);
		agreebt.addListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				taskService.complete(task.getId(),CollectionUtil.singletonMap("agree", true));
				addFolwChat(item);//刷新流程图
				addWorkFlowButtons(item);//刷新按钮
			}
		});

		if("总经理审批".equals(task.getName())
				||"部门经理审批".equals(task.getName())
				){
			Button nobt = new Button("退回发起人");
			this.addFlowAction(nobt);
			nobt.addListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					taskService.complete(task.getId(),CollectionUtil.singletonMap("agree", false));
					addFolwChat(item);//刷新流程图
					addWorkFlowButtons(item);//刷新按钮
				}
			});
		}


	}

	/**
	 * AbstractEntityView类中已经加了事务标签，此处不用再加
	 */
	@Override
	public void commit() {
		Item item = this.getItemDataSource();
		Object id = item.getItemProperty("id").getValue();
		//新建的
		if(id == null){
			//设置默认值
			User user = UserUtil.getCurrentUser();
			item.getItemProperty("createBy").setValue(user);
			item.getItemProperty("createDate").setValue(new Date());
		}
		super.commit();
	}


	private String getBSKey(Item item){
		Object id = item.getItemProperty("id").getValue();
		return ""+id;
	}

	private void addFolwChat(Item item){
		StreamResource diagram = null;
		ProcessInstance pi = null;
		if(getBSKey(item) != null){
			pi = runtimeService.createProcessInstanceQuery()
					.processInstanceBusinessKey(getBSKey(item)).singleResult();
		}
		//TODO .......
//		//生成流程图
//		if(pi != null){
//			//有实例则生成实时图
//			diagram = new ProcessDefinitionImageStreamResourceBuilder()
//			.buildStreamResource(pi, repositoryService, runtimeService);
//		}else{
//			//新建的，直接获得原图
//			ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
//					.processDefinitionKey(getProcessDefinitionKey()).latestVersion().singleResult();
//			diagram = new ProcessDefinitionImageStreamResourceBuilder()
//			.buildStreamResource(processDefinition, repositoryService);
//		}
//
//		Embedded embedded = new Embedded(null, diagram);
//		embedded.setType(Embedded.TYPE_IMAGE);
//		embedded.setSizeUndefined();
//
//		//添加流程图
//		this.addFlowMap(embedded);
	}

	@Override
	public String getProcessDefinitionKey() {
		return "leaveProcess";
	}
	
}
