package com.huataisi.oa.leave;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.vaadin.Application;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;

/**
 * 月请假统计
 * @author 郝威
 *
 */
public class LeaveChartView extends VerticalLayout implements View {
	
	Table table = new Table();
	DateField datebegin = new DateField();
	String begin = null;
	
	public LeaveChartView() {
		table.addContainerProperty("date", String.class, null);
		table.setColumnHeader("date", "日期");
		table.addContainerProperty("customer", String.class, null);
		table.setColumnHeader("customer", "人员");
		table.addContainerProperty("all", Double.class, null);
		table.setColumnHeader("all", "小计");
		table.setSizeFull();
		table.setSelectable(true);
		table.setStyleName(Runo.TABLE_SMALL);
		
		HorizontalLayout actionArea = new HorizontalLayout();
        Label labbegin = new Label("日期：");
        labbegin.setWidth("70px");
        datebegin.setWidth("100px");
        datebegin.setResolution(DateField.RESOLUTION_MONTH);
        datebegin.setDateFormat("yyyy-MM");
        
		//统计按钮
		Button btnSearch = new Button("统计");
		actionArea.addComponent(labbegin);
		actionArea.addComponent(datebegin);
		actionArea.addComponent(btnSearch);
		actionArea.setComponentAlignment(labbegin, Alignment.MIDDLE_RIGHT);
		actionArea.setComponentAlignment(datebegin, Alignment.MIDDLE_RIGHT);
		actionArea.setComponentAlignment(btnSearch, Alignment.MIDDLE_RIGHT);
		
		btnSearch.setClickShortcut(KeyCode.ENTER);
		
		//整体布局
		this.setSizeFull();
		this.addComponent(actionArea);
		this.addComponent(table);
		this.setExpandRatio(table, 100);	
		
		Date date = new Date();
		GregorianCalendar  calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		datebegin.setValue(calendar.getTime());
		int month =calendar.get(Calendar.MONTH)+1;
		begin = calendar.get(Calendar.YEAR)+"-"+(month<10?"0"+month:month);
		
		btnSearch.addListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				GregorianCalendar  calendar = new GregorianCalendar();
				if(datebegin.getValue() != null){
					Date d = (Date)datebegin.getValue();
					calendar.setTime(d);
					calendar.set(Calendar.HOUR_OF_DAY, 0);
					calendar.set(Calendar.MINUTE, 0);
					calendar.set(Calendar.SECOND, 0);
					int month =calendar.get(Calendar.MONTH)+1;
					begin = calendar.get(Calendar.YEAR)+"-"+(month<10?"0"+month:month);
				}else{
					begin = null;
				}
				table.removeAllItems();
				setChartData(begin);				
			}
		});
		setChartData(begin);
//		addSearchEvent();
	}
	
	/**
	 * 反查事件
	 */
	private void addSearchEvent() {
		table.addListener(new ItemClickListener() {
			@Override
			public void itemClick(ItemClickEvent event) {
				
			}
		});
	}
	
	
	/**
	 * 填充数据
	 * @param cusTypeList
	 * @param beginDate
	 * @param endDate
	 * @param userId
	 * @param areaId
	 * @param indId
	 */
	private void setChartData(String beginDate){
		List<Object[]> list = new Leave().getLeaveChart(beginDate);
		Object[] countItem = new Object[table.getColumnHeaders().length];
		for(int i = 0; i < list.size(); i++){
			Object[] item = list.get(i);
			table.addItem(item, i);
			for(int m = 2; m < item.length; m++){
				if(countItem[m] == null){
					countItem[m] = 0.0;
				}
				countItem[m] = (Double)countItem[m]+(Double)item[m];
			}
		}
		if(list != null && list.size() > 0){
			countItem[0] = "合计";
			table.addItem(countItem, list.size());			
		}
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}
	

}
