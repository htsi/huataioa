package com.huataisi.oa.leave;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.transaction.annotation.Transactional;

import com.huataisi.oa.domain.Duty;
import com.huataisi.oa.domain.User;
import com.huataisi.oa.util.Caption;
import com.huataisi.oa.util.IdEntity;
import com.huataisi.oa.util.mail.MailSenderInfo;
import com.huataisi.oa.util.mail.SimpleMailSender;

@Entity
@Table(name="OA_LE_LEAVE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Leave extends IdEntity<Leave> {
	private static final long serialVersionUID = 1L;
	
	@NotNull
	@Caption("请假类型")
	private LeaveType leaveType;

	@NotNull
	@Caption("开始时间")
	private Date startDate;

	@NotNull
	@Caption("结束时间")
	private Date endDate;

	
	private Date backDate;

	@NotNull
	@Min(0)
	@Caption("合计天数")
	private Double countHours;

	@Column(length=2000)
	@Caption("请假原因")
	private String reason;

	@NotNull
	@ManyToOne
	private User createBy;

	@NotNull
	private Date createDate;

	private String flowStatus;

	public String getCreateUsername(){
		return createBy.getUsername();
	}

	/**
	 * 判断是不是职员创建
	 * @return
	 */
	public boolean createByZhiYuan(){
		if(Duty.ZHI_YUAN.equals(createBy.getDuty().getName())){
			return true;
		}
		return false;
	}
	/**
	 * 更改状态
	 * @param task
	 */
	@Transactional
	public void changeStatus(String status){
		this.setFlowStatus(status);
		if("已销假".equals(status)){
			this.setBackDate(new Date());
		}


		this.persist();
	}

	/**
	 * 获得当前人的部门经理
	 * @return
	 */
	public String findJingLi(){
		User u = entityManager().createQuery("select u from User u join u.duty d where u.department = ? and d.name = ?",User.class)
				.setParameter(1, createBy.getDepartment())
				.setParameter(2, Duty.BU_MENG_JING_LI)
				.getSingleResult();
		return u.getUsername();
	}

	public void sendMail(String userName){
		User user = new User().findByUsername(userName);
		
		//这个类主要是设置邮件  
		MailSenderInfo mailInfo = new MailSenderInfo();   
		mailInfo.setMailServerHost("smtp.qq.com");   
		mailInfo.setMailServerPort("25");   
		mailInfo.setValidate(true);   
		mailInfo.setUserName("67892238@qq.com");   
		mailInfo.setPassword("74365020");//您的邮箱密码   
		mailInfo.setFromAddress("67892238@qq.com");   
		mailInfo.setToAddress(user.getEmail());   
		mailInfo.setSubject("外出请假");   
		mailInfo.setContent("您收到一份‘外出请假单’，<a href='http://192.168.1.104/#leave/edit/"+id+"'>请及时办理</a>");   
		//这个类主要来发送邮件  
		SimpleMailSender.sendHtmlMail(mailInfo);//发送文体格式   
	}

	public LeaveType getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getBackDate() {
		return backDate;
	}

	public void setBackDate(Date backDate) {
		this.backDate = backDate;
	}

	public Double getCountHours() {
		return countHours;
	}

	public void setCountHours(Double countHours) {
		this.countHours = countHours;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public User getCreateBy() {
		return createBy;
	}

	public void setCreateBy(User createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getFlowStatus() {
		return flowStatus;
	}

	public void setFlowStatus(String flowStatus) {
		this.flowStatus = flowStatus;
	}

	
	/**
     * 月请假统计
     * @param beginDate
     * @param endDate
     * @return
     */
	public  List<Object[]> getLeaveChart(String beginDate) {
		String select = "select substring(a.startDate,1,7),b.truename,sum(a.countHours) from Leave a left join a.createBy b where a.leaveType not in ('1','2') ";		
		if(beginDate != null){
			select += " and substring(a.startDate,1,7) = '"+beginDate+"'";
		}
		select += " group by substring(a.startDate,1,7),a.createBy order by substring(a.startDate,1,7)";
		return entityManager().createQuery(select).getResultList();
	}
}
