package com.huataisi.oa.domain;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.huataisi.oa.util.IdEntity;

/**
 * 权限组.
 */
@Entity
@Table(name ="OA_ROLE", uniqueConstraints = { @UniqueConstraint(columnNames = {"name"})})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Role extends IdEntity<Role>{
	private static final long serialVersionUID = 1L;

	public final static String ADMIN = "admin";

	public static final String USER = "user";
	
	@NotNull
	private String name;

	private String remark;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private Set<Menu> menus = new LinkedHashSet<Menu>();
	
	@ManyToMany(mappedBy="roles",fetch=FetchType.EAGER)
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private Set<User> users = new LinkedHashSet<User>();

	public Role(String name) {
		this.name = name;
	}


	public Role(String name, String desc) {
		this.name = name;
		this.remark = desc;
	}


	public Role() {
	}


	@Transient
	public List<String> toPermissionNames() {
		List<String> permissionNameList = new ArrayList<String>();
		for (Menu menu : menus) {
			permissionNameList.add(menu.getName());
		}
		return permissionNameList;
	}


	public void addMenu(Menu menu) {
		menus.add(menu);
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public Set<Menu> getMenus() {
		return menus;
	}


	public void setMenus(Set<Menu> menus) {
		this.menus = menus;
	}


	public Set<User> getUsers() {
		return users;
	}


	public void setUsers(Set<User> users) {
		this.users = users;
	}


}
