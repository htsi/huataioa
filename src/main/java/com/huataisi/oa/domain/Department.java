package com.huataisi.oa.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.huataisi.oa.util.Caption;
import com.huataisi.oa.util.IdEntity;

@Entity
@Table(name="OA_DEPARTMENT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Department extends IdEntity<Department>{
	
	private static final long serialVersionUID = 1L;

	public Department(String name,Department parent) {
		this.name = name;
		this.parent = parent;
	}

	public Department() {
	}

	@NotNull
    @Size(min = 2)
	@Caption("部门名称")
    private String name;
	
    @ManyToOne
    @Caption("上级部门")
    private Department parent;
    
    @OneToMany(mappedBy="department")
    private Set<User> users = new HashSet<User>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Department getParent() {
		return parent;
	}

	public void setParent(Department parent) {
		this.parent = parent;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
    
    
    @Override
    public String toString(){
    	return name;
    }
}
