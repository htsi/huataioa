package com.huataisi.oa.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.huataisi.oa.util.IdEntity;


@Entity
@Table(name = "OA_MENU", uniqueConstraints = { @UniqueConstraint(columnNames = {
		"name", "viewName" }) })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Menu extends IdEntity<Menu>{
	private static final long serialVersionUID = 1L;

	public Menu(String name, MenuType mtype, Menu parent, String viewName,
			String viewClass) {
		super();
		this.name = name;
		this.viewName = viewName;
		this.mtype = mtype;
		this.parent = parent;
		this.viewClass = viewClass;
	}

	public Menu() {
	}

	@NotNull
	@Size(min = 2)
	private String name;

	private String viewName;

	private String viewClass;

	@NotNull
	@Enumerated(EnumType.STRING)
	private MenuType mtype;

	private String icon;

	@ManyToOne
	private Menu parent;

//	@ManyToMany(mappedBy = "menus", fetch = FetchType.EAGER)
//	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
//	private Set<Role> roles = new HashSet<Role>();

	public boolean isLeaf() {
		if (MenuType.MODULE.equals(mtype)) {
			return false;
		}
		return true;
	}
	
	public String getIcon(){
		if(StringUtils.isBlank(icon)){
			return "icons/item.png";
		}
		return icon;
	}

	public List<Menu> fetchByUserName(String username) {
		String ql = "select m from Role r join r.menus as m join r.users as u where u.username = ? ";
		return entityManager().createQuery(ql, Menu.class)
				.setParameter(1, username).getResultList();
	}

	public List<Menu> findTopMenus() {
		return entityManager().createQuery("from Menu where parent is null",
				Menu.class).getResultList();
	}

	public List<Menu> findAllModules() {
		return entityManager()
				.createQuery("from Menu where mtype = ?", Menu.class)
				.setParameter(1, MenuType.MODULE).getResultList();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Menu other = (Menu) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	public String toString(){
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	public String getViewClass() {
		return viewClass;
	}

	public void setViewClass(String viewClass) {
		this.viewClass = viewClass;
	}

	public MenuType getMtype() {
		return mtype;
	}

	public void setMtype(MenuType mtype) {
		this.mtype = mtype;
	}

	public Menu getParent() {
		return parent;
	}

	public void setParent(Menu parent) {
		this.parent = parent;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
}
