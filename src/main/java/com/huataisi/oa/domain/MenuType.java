package com.huataisi.oa.domain;

import java.io.Serializable;

public enum MenuType implements Serializable{
    MODULE, APPLICATION, ACTION, LINK
}