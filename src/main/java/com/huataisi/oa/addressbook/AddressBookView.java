package com.huataisi.oa.addressbook;

import com.huataisi.oa.domain.User;
import com.huataisi.oa.util.ContainerUtils;
import com.vaadin.Application;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.util.filter.Like;
import com.vaadin.data.util.filter.Not;
import com.vaadin.data.util.filter.Or;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class AddressBookView  extends CustomComponent implements View {

	final JPAContainer<User> container = ContainerUtils.createJPAContainer(User.class);
	public AddressBookView() {
		
		
		final Not not = new Not(new Like("username", "admin"));
		Table table = new Table(null,container);
		container.addContainerFilter(not);
		table.setSelectable(true);
		table.setVisibleColumns(new Object[]{"truename","department","duty","phone","mobile","qq","email"});
		
        table.setColumnHeader("truename", "姓名");
        table.setColumnHeader("phone", "手机");
        table.setColumnHeader("mobile", "固话");
        table.setColumnHeader("department", "部门");
        table.setColumnHeader("duty", "职务");
        table.setColumnHeader("qq", "QQ");
        table.setColumnHeader("email", "Email");
        
        table.setSizeFull();
        table.setHeight("2000px");
        
        //整体布局
        VerticalLayout mainLayout  = new VerticalLayout();
//        mainLayout.setStyleName("oa-VerticalLayout-padding-top-buttom");
        HorizontalLayout actionlayout = new HorizontalLayout();
        
      
        
        //查找框
  		final TextField searchField = new TextField();
  		searchField.setImmediate(true);
  		searchField.setWidth("300px");
  		searchField.setInputPrompt("请输入姓名或电话或创建者简拼或QQ号码并按回车查找");
  		actionlayout.addComponent(searchField);
       
  		
  		//查找按钮
		Button searchb = new Button("搜索");
		searchb.setImmediate(true);
		actionlayout.addComponent(searchb);
		actionlayout.setComponentAlignment(searchField, Alignment.MIDDLE_CENTER);
		searchb.addListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				String textFilter = (String) searchField.getValue();
				
				if(textFilter != null && !"".equals(textFilter)){
					Or or = new Or(
							new Like("username", "%"+textFilter+"%",false)
							,new Like("truename", "%"+textFilter+"%",false)
							,new Like("qq", "%"+textFilter+"%",false)
							,new Like("phone", "%"+textFilter+"%",false)
							,new Like("email", "%"+textFilter+"%",false)
							);
					container.setApplyFiltersImmediately(false);
					container.removeAllContainerFilters();
					container.addContainerFilter(or);
					container.addContainerFilter(not);
					container.applyFilters();
				}else{
					container.setApplyFiltersImmediately(false);
					container.removeAllContainerFilters();
					container.addContainerFilter(not);
					container.applyFilters();
				}
			}
		});
		searchb.setClickShortcut(KeyCode.ENTER);
    
		mainLayout.addComponent(actionlayout);
		mainLayout.addComponent(table);
		
	    this.setCompositionRoot(mainLayout);
	    
	    
	}


	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}
   
}
