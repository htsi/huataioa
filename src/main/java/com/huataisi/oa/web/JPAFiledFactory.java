package com.huataisi.oa.web;
import java.util.Collection;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TableFieldFactory;
import com.vaadin.ui.TextField;
@SuppressWarnings("serial")
public class JPAFiledFactory implements FormFieldFactory, TableFieldFactory{
	final Logger logger = LoggerFactory.getLogger(JPAFiledFactory.class);

	private static final JPAFiledFactory instance = new JPAFiledFactory();

	/**
	 * Singleton method to get an instance of DefaultFieldFactory.
	 * 
	 * @return an instance of DefaultFieldFactory
	 */
	public static JPAFiledFactory get() {
		return instance;
	}

	private JPAFiledFactory() {
	}
	@Override
	public Field createField(Item item, Object propertyId, Component uiContext) {
		logger.debug("createField item");
		Class<?> type = item.getItemProperty(propertyId).getType();
		Field field = createFieldByPropertyType(type);
		field.setCaption(createCaptionByPropertyId(propertyId));
		return field;
	}
	@Override
	public Field createField(Container container, Object itemId, Object propertyId, Component uiContext) {
		logger.debug("createField container");
		Property containerProperty = container.getContainerProperty(itemId,propertyId);
		Class<?> type = containerProperty.getType();
		
		Field field = createFieldByPropertyType(type);
		field.setCaption(createCaptionByPropertyId(propertyId));
		return field;
	}


	public static Field createFieldByPropertyType(Class<?> type) {
		// Null typed properties can not be edited
		if (type == null) {
			return null;
		}

		// Item field
		if (Item.class.isAssignableFrom(type)) {
			return new Form();
		}

		// Date field
		if (Date.class.isAssignableFrom(type)) {
			final DateField df = new DateField();
			df.setResolution(DateField.RESOLUTION_DAY);
			return df;
		}

		// Boolean field
		if (Boolean.class.isAssignableFrom(type)) {
			return new CheckBox();
		}
		
		// Number field
		if (Number.class.isAssignableFrom(type)) {
			return new TextField();
		}
		// 基本类型 field
		if (type.isPrimitive()) {
			return new TextField();
		}
		// 集合类型 field
		if (Collection.class.isAssignableFrom(type)) {
			//TODU
			return new TextField();
		}
		// String field
		if (String.class.isAssignableFrom(type)) {
			//TODO 集合类
			return new TextField();
		}
		// Enum field
		if (Enum.class.isAssignableFrom(type)) {
			ComboBox cb = new ComboBox();
			return cb;
		}
		// Entity field
		if (Object.class.isAssignableFrom(type)) {
			return new ComboBox();
		}

		return null;
	}
	
	
	/**
	 * If name follows method naming conventions, convert the name to spaced
	 * upper case text. For example, convert "firstName" to "First Name"
	 * 
	 * @param propertyId
	 * @return the formatted caption string
	 */
	public static String createCaptionByPropertyId(Object propertyId) {
		String name = propertyId.toString();
		if (name.length() > 0) {

			if (name.indexOf(' ') < 0
					&& name.charAt(0) == Character.toLowerCase(name.charAt(0))
					&& name.charAt(0) != Character.toUpperCase(name.charAt(0))) {
				StringBuffer out = new StringBuffer();
				out.append(Character.toUpperCase(name.charAt(0)));
				int i = 1;

				while (i < name.length()) {
					int j = i;
					for (; j < name.length(); j++) {
						char c = name.charAt(j);
						if (Character.toLowerCase(c) != c
								&& Character.toUpperCase(c) == c) {
							break;
						}
					}
					if (j == name.length()) {
						out.append(name.substring(i));
					} else {
						out.append(name.substring(i, j));
						out.append(" " + name.charAt(j));
					}
					i = j + 1;
				}

				name = out.toString();
			}
		}
		return name;
	}

}
