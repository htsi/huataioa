package com.huataisi.oa.web;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.vaadin.data.Property;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
public class DateColumGenerator implements Table.ColumnGenerator{
	private static final long serialVersionUID = 1L;
	SimpleDateFormat sdf ;
	
	public DateColumGenerator(String format){
		sdf = new SimpleDateFormat(format);
	}
	
	public DateColumGenerator(){
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	}
	
	@Override
	public Object generateCell(Table source, Object itemId, Object columnId) {
		Property p = source.getItem(itemId).getItemProperty(columnId);
		Date date = (Date) p.getValue();
		String sv = date==null?"":sdf.format(date);
		return new Label(sv);
	}

}
