package com.huataisi.oa.web;

import java.util.Collection;
import java.util.Collections;

import org.springframework.transaction.annotation.Transactional;

import com.huataisi.oa.util.EntityForm;
import com.vaadin.addon.beanvalidation.BeanValidationForm;
import com.vaadin.data.Item;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

public class AutomaticEntityForm<T> extends CustomComponent implements EntityEditor {

    private Button deleteButton;
    private Button cancelButton;
    private Button saveButton;

    private VerticalLayout formPanel;
    private EntityForm<T> form;
    private VerticalLayout flowMap = new VerticalLayout();
    private HorizontalLayout flowbuttons = new HorizontalLayout();

    public AutomaticEntityForm(Class<T> entityClass) {
        form = new EntityForm<T>(entityClass);

        // form editor layout
        HorizontalLayout wrapper = new HorizontalLayout();
        wrapper.setMargin(true);
        formPanel = new VerticalLayout();
//        formPanel.setIcon(new ThemeResource("img/edit-icon.png"));
        formPanel.addComponent(getForm());
        wrapper.addComponent(formPanel);
        wrapper.addComponent(flowMap);

        setCompositionRoot(wrapper);

        // immediate validation but no changes to the underlying entity
        getForm().setImmediate(true);

        createFormFooter();

        // make saving the form the default action on Enter keypress
        getSaveButton().setClickShortcut(KeyCode.ENTER);
    }

    public void addSaveActionListener(ClickListener listener) {
        getSaveButton().addListener(listener);
    }

    public void addCancelActionListener(ClickListener listener) {
        getCancelButton().addListener(listener);
    }

    public void addDeleteActionListener(ClickListener listener) {
        getDeleteButton().addListener(listener);
    }

    public void setSaveAllowed(boolean canSave) {
        getSaveButton().setVisible(canSave);
//        getCancelButton().setVisible(canSave);
        getSaveButton().setEnabled(canSave);
//        getCancelButton().setEnabled(canSave);

        // do not change the enabled state of the delete button
        getForm().getLayout().setEnabled(canSave);
    }

    public void setDeleteAllowed(boolean canDelete) {
        getDeleteButton().setVisible(canDelete);
        getDeleteButton().setEnabled(canDelete);
    }
    @Transactional
    public void commit() {
        getForm().commit();
    }

    public void setItemDataSource(Item item) {
        if (null != item) {
            getForm().setItemDataSource(item, getItemPropertyIds(item));
        } else {
            getForm().setItemDataSource(null);
        }

        // may reuse form and button, so clear any old error messages
        getSaveButton().setComponentError(null);
        getForm().setComponentError(null);

        // don't show validation errors before user tries to commit the form
        getForm().setValidationVisible(false);
    }

    /**
     * Return item property IDs to show on the form.
     * 
     * This method can be overridden to hide specific properties or to customize
     * the order of the fields.
     * 
     * @param item
     * @return Collection of property identifiers for the item
     */
    protected Collection<?> getItemPropertyIds(Item item) {
        if (null == item) {
            return Collections.emptyList();
        }
        return item.getItemPropertyIds();
    }

    public Item getItemDataSource() {
        return getForm().getItemDataSource();
    }

    public void setCommitErrorMessage(String message) {
    }

    protected EntityForm<T> getForm() {
        return form;
    }

    protected Button getSaveButton() {
        if (saveButton == null) {
            saveButton = new Button("保存");
            saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
        }
        return saveButton;
    }

    protected Button getCancelButton() {
        if (cancelButton == null) {
            cancelButton = new Button("取消");
        }
        return cancelButton;
    }

    protected Button getDeleteButton() {
        if (deleteButton == null) {
            deleteButton = new Button("删除");
        }
        return deleteButton;
    }

    protected void createFormFooter() {
    	VerticalLayout footer = new VerticalLayout();
    	getForm().setFooter(footer);
    	footer.setSpacing(true);
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	
        HorizontalLayout formbuttons = new HorizontalLayout();
        formbuttons.setSpacing(true);
        formbuttons.addComponent(getSaveButton());
        formbuttons.addComponent(getCancelButton());
        formbuttons.addComponent(getDeleteButton());
        
        buttons.addComponent(formbuttons);
        buttons.addComponent(flowbuttons);
        flowbuttons.setSpacing(true);
        
        footer.addComponent(buttons);
    }
    
    public void addFlowMap(Component c){
    	flowMap.removeAllComponents();
    	flowMap.addComponent(c);
    }
    public void addFlowAction(Component  c){
    	this.flowbuttons.addComponent(c);
    }
    public void clearFlowActions(){
    	this.flowbuttons.removeAllComponents();
    }

    @Override
    public void setCaption(String caption) {
        formPanel.setCaption(caption);
    }

    @Override
    public void focus() {
        getForm().focus();
    }

    public void refresh() {
    	// nothing to do
    }

    public boolean isModified() {
        return getForm().isModified();
    }

	@Override
	public String getProcessDefinitionKey() {
		return null;
	}

}