package com.huataisi.oa.web.ui;

import com.huataisi.oa.domain.User;
import com.huataisi.oa.util.AutoEntityView;

@SuppressWarnings("serial")
public class UserView extends AutoEntityView<User> {
	
    public UserView() {
		super(User.class);
	}

}
