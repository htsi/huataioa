package com.huataisi.oa.web.ui;

import com.huataisi.oa.domain.Duty;
import com.huataisi.oa.util.AutoEntityView;
import com.vaadin.ui.Table;

@SuppressWarnings("serial")
public class DutyView extends AutoEntityView<Duty> {

    public DutyView() {
		super(Duty.class);
	}

	@Override
    protected void configureTable(Table table) {
        table.setVisibleColumns(new String[]{"name","remark"});

        table.setColumnHeader("name", "职务名称");
        table.setColumnHeader("remark", "职务描述");
        
        table.setWidth(100,UNITS_PERCENTAGE);
    }


}
