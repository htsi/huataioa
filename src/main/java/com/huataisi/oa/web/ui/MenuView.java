package com.huataisi.oa.web.ui;

import com.huataisi.oa.domain.Menu;
import com.huataisi.oa.util.AutoEntityView;
import com.vaadin.ui.Table;

@SuppressWarnings("serial")
public class MenuView extends AutoEntityView<Menu> {
    
    public MenuView() {
		super(Menu.class);
	}

	@Override
    protected void configureTable(Table table) {
         table.setVisibleColumns(new String[]{"icon","name","viewName","viewClass","mtype","parent"});
         
         table.setWidth(100,UNITS_PERCENTAGE);
    }
    
}
