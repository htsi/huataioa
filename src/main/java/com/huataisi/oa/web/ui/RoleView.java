package com.huataisi.oa.web.ui;

import com.huataisi.oa.domain.Role;
import com.huataisi.oa.util.AutoEntityForm;
import com.huataisi.oa.util.AutoEntityView;
import com.huataisi.oa.util.LayoutField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Table;

@SuppressWarnings("serial")
public class RoleView extends AutoEntityView<Role> {

    public RoleView() {
		super(Role.class);
	}
	@Override
	public void configForm(AutoEntityForm<Role> entityForm){
		entityForm.addTabVisibleFields("详细信息", new GridLayout(2,3)
			,"name",new LayoutField("remark",1,3),"menus","users");
    }
	@Override
    protected void configureTable(Table table) {
        table.setVisibleColumns(new String[]{"name","remark"});


        table.setColumnHeader("name", "角色名称");
        table.setColumnHeader("remark", "角色描述");
        table.setWidth(100,UNITS_PERCENTAGE);
        
    }

}
