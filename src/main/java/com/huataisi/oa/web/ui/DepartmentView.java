package com.huataisi.oa.web.ui;

import com.huataisi.oa.domain.Department;
import com.huataisi.oa.util.AutoEntityView;
import com.vaadin.ui.Table;

@SuppressWarnings("serial")
public class DepartmentView extends AutoEntityView<Department> {


    public DepartmentView() {
		super(Department.class);
	}

	@Override
    protected void configureTable(Table table) {
        table.setVisibleColumns(new String[]{"name","parent"});
    }

}
