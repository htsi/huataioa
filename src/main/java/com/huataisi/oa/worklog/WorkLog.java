package com.huataisi.oa.worklog;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.huataisi.oa.domain.User;
import com.huataisi.oa.util.Caption;
import com.huataisi.oa.util.IdEntity;

@Entity
@Table(name="OA_WL_WORKLOG")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WorkLog extends IdEntity<WorkLog>{
	private static final long serialVersionUID = 1L;

	@Caption("\u65E5\u5FD7\u65F6\u95F4")
	private Date logDate;
	
	@Caption("\u65E5\u5FD7\u5185\u5BB9")
	@Column(length=20000)
	private String content;
	
	@Caption("\u6D41\u7A0B\u72B6\u6001")
	private String flowStatus;

	@Caption("\u521B\u5EFA\u4EBA")
	@ManyToOne
	@NotNull
	private User createBy;
	
	
	@Caption("\u521B\u5EFA\u65F6\u95F4")
	private Date createDate;


	public Date getLogDate() {
		return logDate;
	}


	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getFlowStatus() {
		return flowStatus;
	}


	public void setFlowStatus(String flowStatus) {
		this.flowStatus = flowStatus;
	}


	public User getCreateBy() {
		return createBy;
	}


	public void setCreateBy(User createBy) {
		this.createBy = createBy;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
}
