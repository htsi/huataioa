package com.huataisi.oa.worklog;

import com.huataisi.oa.util.AutoEntityView;

@SuppressWarnings("serial")
public class WorkLogView extends AutoEntityView<WorkLog> {
	public WorkLogView() {
		super(WorkLog.class);
	}
}
