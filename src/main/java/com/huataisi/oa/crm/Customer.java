package com.huataisi.oa.crm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.huataisi.oa.domain.User;
import com.huataisi.oa.util.Caption;
import com.huataisi.oa.util.IdEntity;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name ="OA_CRM_CUSTOMER", uniqueConstraints = { @UniqueConstraint(columnNames = {"name"}) })
public class Customer extends IdEntity<Customer>{
	private static final long serialVersionUID = 1L;

	public Customer(String name, Integer scale, Industry industry,Area area,
			CusType customerType, User createBy,Date createDate) {
		this.name = name;
		this.scale = scale;
		this.industry = industry;
		this.customerType = customerType;
		this.createBy = createBy;
		this.createDate = createDate;
		this.area = area;
	}

	public Customer(){
		createDate = new Date();
	}

	@Caption("\u5BA2\u6237\u540D\u79F0")
	@NotNull
	private String name;
	@Caption("\u89C4\u6A21")
	@Max(10000000)
	private Integer scale;
	@Caption("\u884C\u4E1A")
	@ManyToOne
	private Industry industry;
	@Caption("\u7535\u8BDD")
	private String phone;
	@Caption("\u4F7F\u7528\u8F6F\u4EF6\uFF1F")
	private boolean useSoft;
	@Caption("\u7F51\u5740")
	private String url;
	@Caption("\u5730\u5740")
	private String address;
	@Caption("\u5907\u6CE8")
	@Column(length=2000)
	private String remark;
	@Caption("\u4E0B\u6B21\u8054\u7CFB\u65F6\u95F4")
	private Date nextContactDate;

	@Caption("\u521B\u5EFA\u4EBA")
	@ManyToOne
	@NotNull
	private User createBy;

	@Caption("\u521B\u5EFA\u65F6\u95F4")
	private Date createDate;

	@Caption("\u6700\u540E\u8DDF\u8E2A\u65F6\u95F4")
	private Date lastTrackDate;
	@Caption("\u533A\u57DF")
	@ManyToOne
	private Area area;

	@Caption("\u5BA2\u6237\u610F\u5411")
	@ManyToOne
	private CusType customerType;

	@Caption("\u4F9B\u5E94\u94FE\u4F4D\u7F6E")
	private CusCategory category;
	
	@OneToMany(mappedBy="customer")
	private List<Track> tracks = new ArrayList<Track>();

	@OneToMany(mappedBy="customer")
	private List<Contact> contacts = new ArrayList<Contact>();

	@OneToMany(mappedBy="customer")
	private List<CusTypeHistory> typeTrack = new ArrayList<CusTypeHistory>();

	/**
	 * 客户总量(人员区域行业分组)
	 * @param userId
	 * @param areaId
	 * @param indId
	 * @param whereCause
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getAllCustomerChart(String userId, String areaId, String indId, String whereCause) {
		List<CusType> types = new CusType().findAll();
		String select = "select b.truename,c.name,i.name";
		for(CusType type : types){
			select += ", sum(case when t="+type.getId()+" then 1 else 0 end)";
		}
		select += ", sum(case when t is null then 1 else 0 end)";

		String ql= select+ ",count(a) from Customer a left join a.createBy b left join a.area c left join a.industry i left join a.customerType t where b in "+whereCause;
		if(userId != null){
			ql += " and b.id ="+userId;
		}
		if(areaId != null){
			ql += " and c.id ="+areaId;
		}
		if(indId != null){
			ql += " and i.id ="+indId;
		}

		ql += " group by a.createBy,a.area,a.industry";
		return entityManager().createQuery(ql).getResultList();
	}


	/**
	 * 电话总量(日期人员区域行业分组)
	 * @param beginDate
	 * @param endDate
	 * @param userId
	 * @param areaId
	 * @param indId
	 * @param bound
	 * @param whereCause
	 * @return
	 */
	public List<Object[]> getCustomerTelChart(String beginDate, String endDate, String userId, String areaId, String indId, String bound, String whereCause) {
		List<CusType> types = new CusType().findAll();
		String select = "select b.truename,c.name,i.name,substring(tk.ttime,1,10)";
		for(CusType type : types){
			select += ",sum(case when t="+type.getId()+" then 1 else 0 end)";
		}
		String ql = select + ",count(a) from Customer a left join a.createBy b left join a.area c left join a.industry i left join a.customerType t right join a.tracks tk where b in "+whereCause;
		if(beginDate != null){
			ql += " and tk.ttime >= '"+beginDate+"'";
		}
		if(endDate != null){
			ql += " and tk.ttime < '"+endDate+"'";
		}
		if(userId != null){
			ql += " and b.id ="+userId;
		}
		if(areaId != null){
			ql += " and c.id ="+areaId;
		}
		if(indId != null){
			ql += " and i.id ="+indId;
		}
		if("新增".equals(bound)){
			ql += " and substring(tk.ttime,1,10) = substring(a.createDate,1,10)";
		}else if("回访".equals(bound)){
			ql += " and substring(tk.ttime,1,10) <> substring(a.createDate,1,10) and tk.ttime > a.createDate";			
		}

		ql += " group by substring(tk.ttime,1,10),a.createBy,a.area,a.industry";
		return entityManager().createQuery(ql).getResultList();
	}
	
	/**
	 * 客户ids(日期人员区域行业分组)
	 * @param beginDate
	 * @param endDate
	 * @param userId
	 * @param areaId
	 * @param indId
	 * @param bound
	 * @param whereCause
	 * @return
	 */
	public List getTelCustomerIds(String begin, String end, String userId, String areaId, String indId, 
			String truename, String area, String ind, String bound, String cusType, String whereCause) {
		List<CusType> types = new CusType().findAll();
		String ql = "select a.id from Customer a left join a.createBy b left join a.area c left join a.industry i left join a.customerType t right join a.tracks tk " +
				"where b in "+whereCause;
		
		if(truename != null){
			ql += " and b.truename ='"+truename+"'";
		}
		if(cusType != null){
			ql += " and t ="+cusType;
		}
		if(area != null){
			ql += " and c.name='"+area+"'";
		}else if(truename != null && area == null){
			ql += " and a.area is null";			
		}
		if(ind != null){
			ql += " and i.name='"+ind+"'";
		}else if(truename != null && ind == null){
			ql += " and a.industry is null";			
		}
		if(userId != null){
			ql += " and b.id ="+userId;
		}
		if(areaId != null){
			ql += " and c.id ="+areaId;
		}
		if(indId != null){
			ql += " and i.id ="+indId;
		}
		if(begin != null){
			ql += " and tk.ttime >= '"+begin+"'";
		}
		if(end != null){
			ql += " and tk.ttime < '"+end+"'";
		}
		if("新增".equals(bound)){
			ql += " and substring(tk.ttime,1,10) = substring(a.createDate,1,10)";
		}else if("回访".equals(bound)){
			ql += " and substring(tk.ttime,1,10) <> substring(a.createDate,1,10) and tk.ttime > a.createDate";			
		}
		
		return entityManager().createQuery(ql).getResultList();
	}

	/**
	 * 客户重要性转化总量(日期人员区域行业分组)
	 * @param beginDate
	 * @param endDate
	 * @param userId
	 * @param areaId
	 * @param indId
	 * @param whereCause
	 * @return
	 */
	public List<Object[]> getCustomerConvertChart(String beginDate, String endDate, String userId, String areaId,
			String indId,String whereCause) {
		List<CusType> types = new CusType().findAll();
		String select = "select b.truename,c.name,i.name,substring(t.changeDate,1,10)";
		for(CusType type : types){
			select += ",sum(case when t.ctype="+type.getId()+" then 1 else 0 end)";
		}
		String ql = select + ",count(a) from Customer a left join a.createBy b left join a.area c left join a.industry i right join a.typeTrack t where b in "+whereCause;
		if(beginDate != null){
			ql += " and t.changeDate >= '"+beginDate+"'";
		}
		if(endDate != null){
			ql += " and t.changeDate < '"+endDate+"'";
		}
		if(userId != null){
			ql += " and b.id ="+userId;
		}
		if(areaId != null){
			ql += " and c.id ="+areaId;
		}
		if(indId != null){
			ql += " and i.id ="+indId;
		}

		ql += " group by substring(t.changeDate,1,10),a.createBy,a.area,a.industry";
		return entityManager().createQuery(ql).getResultList();
	}

	
	
	//get and set
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getScale() {
		return scale;
	}

	public void setScale(Integer scale) {
		this.scale = scale;
	}

	public Industry getIndustry() {
		return industry;
	}

	public void setIndustry(Industry industry) {
		this.industry = industry;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean isUseSoft() {
		return useSoft;
	}

	public void setUseSoft(boolean useSoft) {
		this.useSoft = useSoft;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getNextContactDate() {
		return nextContactDate;
	}

	public void setNextContactDate(Date nextContactDate) {
		this.nextContactDate = nextContactDate;
	}

	public User getCreateBy() {
		return createBy;
	}

	public void setCreateBy(User createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getLastTrackDate() {
		return lastTrackDate;
	}

	public void setLastTrackDate(Date lastTrackDate) {
		this.lastTrackDate = lastTrackDate;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public CusType getCustomerType() {
		return customerType;
	}

	public void setCustomerType(CusType customerType) {
		this.customerType = customerType;
	}

	public CusCategory getCategory() {
		return category;
	}

	public void setCategory(CusCategory category) {
		this.category = category;
	}

	public List<Track> getTracks() {
		return tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public List<CusTypeHistory> getTypeTrack() {
		return typeTrack;
	}

	public void setTypeTrack(List<CusTypeHistory> typeTrack) {
		this.typeTrack = typeTrack;
	}
	

}
