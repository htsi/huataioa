package com.huataisi.oa.crm;


import com.huataisi.oa.util.AutoEntityForm;
import com.huataisi.oa.util.AutoEntityView;
import com.huataisi.oa.util.ContainerUtils;
import com.huataisi.oa.util.LayoutField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Table;

@SuppressWarnings("serial")
public class AreaView extends AutoEntityView<com.huataisi.oa.crm.Area> {
    public AreaView() {
		super(Area.class);
	}
    
	@Override
	public void configForm(AutoEntityForm<Area> entityForm){
		entityForm.addTabVisibleFields("详细信息", new GridLayout(2,3)
			,"name",new LayoutField("users",1,3),"parent","leaf");
    }

	@Override
    protected void configureTable(Table table) {
        
        table.setVisibleColumns(new Object[]{"name","users","leaf","parent"});
        table.setColumnExpandRatio("users", 1);
        table.setColumnExpandRatio("leaf", 1);
        table.setColumnExpandRatio("name", 1);
        table.setColumnExpandRatio("parent", 1);
    }
}
