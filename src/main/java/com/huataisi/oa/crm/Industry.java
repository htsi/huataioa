package com.huataisi.oa.crm;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.huataisi.oa.util.Caption;
import com.huataisi.oa.util.IdEntity;

@SuppressWarnings("serial")
@Entity()
@Table(name ="OA_CRM_industry", uniqueConstraints = { @UniqueConstraint(columnNames = {"name"}) })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Industry extends IdEntity<Industry>{
	
	public Industry() {
	}
	public Industry(String name) {
		this.name = name;
	}

	@NotNull
	@Caption("\u540D\u79F0")
	private String name;
	
	
	@Override
    public String toString(){
    	return name;
    }
	
	public Industry findIndustryByName(String industryName){
		List<Industry> industrys = entityManager()
				.createQuery("select i from Industry i where i.name = ? ",Industry.class)
				.setParameter(1, industryName).getResultList();
		if (industrys == null || industrys.size() == 0) {
			return null;
		}else {
			return industrys.get(0);
		}
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
