package com.huataisi.oa.crm;

import com.huataisi.oa.util.AutoEntityView;

@SuppressWarnings("serial")
public class CusTypeView extends AutoEntityView<CusType> {

	public CusTypeView() {
		super(CusType.class);
	}

}
