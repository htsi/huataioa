package com.huataisi.oa.crm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.huataisi.oa.util.Caption;
import com.huataisi.oa.util.IdEntity;
import com.huataisi.oa.util.Sex;

@Entity
@Table(name="OA_CRM_Contact")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Contact  extends IdEntity<Contact> {
	
	private static final long serialVersionUID = 1L;
	
	public Contact() {
	}
	public Contact(String name) {
		this.name = name;
	}

	@Caption("\u59D3\u540D")
	private String name;
	
	@Caption("\u90E8\u95E8")
	private String department;
	
	@Caption("\u804C\u52A1")
	private String duty;
	
	@Caption("\u6027\u522B")
	private Sex sex;
	
	@Caption("\u624B\u673A")
	private String phone;
	
	@Caption("\u5EA7\u673A")
	private String tel;
	
	@Caption("Email")
	private String email;
	
	@Caption("QQ")
	private String qq;
	
	@Caption("\u5907\u6CE8")
	@Column(length=2000)
	private String remark;
	
	@ManyToOne()
	@JoinColumn(name="customer_id")  
	private Customer customer;
	
	@Override
    public String toString(){
    	return name;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDuty() {
		return duty;
	}

	public void setDuty(String duty) {
		this.duty = duty;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
}
