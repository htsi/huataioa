package com.huataisi.oa.crm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huataisi.oa.domain.Duty;
import com.huataisi.oa.domain.User;
import com.huataisi.oa.security.UserUtil;
import com.huataisi.oa.util.AutoEntityView;
import com.huataisi.oa.util.ContainerUtils;
import com.huataisi.oa.web.EntityEditor;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.filter.And;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.data.util.filter.Compare.Equal;
import com.vaadin.data.util.filter.Like;
import com.vaadin.data.util.filter.Not;
import com.vaadin.data.util.filter.Or;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Tree;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Runo;

@SuppressWarnings("serial")
public class CustomerView extends AutoEntityView<Customer> {

	Logger logger = LoggerFactory.getLogger(CustomerView.class);
	private TabSheet tabSheet;
	private Tree areaTree;
	private Table typeTable;

	//查找用变量
	private Area searArea;
	private CusType searType;
	private Date searNextPanA;
	private Date searNextPanB;
	private String searText;
	private DateField nextPlanA;
	private DateField nextPlanB;
	private TextField searchField;
	private ComboBox userBox;

	private static Filter searchFilter;//反查用，此处用静态可能会用问题

	public CustomerView() {
		super(Customer.class);
		setFormHeight(310);
	}
	
	@Override
	protected EntityEditor createForm() {
		return new CustomerFormA();
	}

	
	@Override
	protected void configureTable(Table table) {
		filterData();

		table.setVisibleColumns(new String[]{"createDate","name", "phone","customerType","nextContactDate","area","createBy"});
		table.setColumnExpandRatio("phone", 3);
		table.setColumnExpandRatio("name", 1);
	}
	/**
	 * 设置导航区域组件
	 */
	@Override
	protected Component setNaviComponent(){
		final JPAContainer<CusType> typeContainer = ContainerUtils.createJPAContainer(CusType.class);
		final JPAContainer<Area> areaContainer = ContainerUtils.createJPAContainer(Area.class);

		setNavigateAreaWith(100);
		//tabsheets
		tabSheet = new TabSheet();
		tabSheet.setImmediate(false);
		tabSheet.setSizeFull();
		tabSheet.setStyleName(Runo.TABSHEET_SMALL);

		// typeTable
		typeTable = new Table(null,typeContainer);
		typeTable.setSelectable(true);
		typeTable.setImmediate(true);
		typeTable.setWidth("100.0%");
		typeTable.setHeight("100.0%");
		typeTable.setVisibleColumns(new Object[]{"name"});
		typeTable.setStyleName("oa-tableHeader");

		typeTable.addListener(new ValueChangeListener() {
			//点击分类时的事件
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object id = event.getProperty().getValue();
				searType = id == null ? null : typeContainer.getItem(id).getEntity();
				filterData();
			}
		});


		// areaTree
		areaTree = new Tree(null,areaContainer);
		areaTree.setItemCaptionPropertyId("name");
		areaTree.setImmediate(true);
		areaTree.setWidth("100.0%");
		areaTree.setHeight("-1px");
		for(Object id : areaTree.getItemIds()){
			areaTree.expandItem(id);
		}
		areaTree.addListener(new ValueChangeListener() {
			//点击区域分布时的过渡事件
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object id = event.getProperty().getValue();
				searArea = id == null ? null : areaContainer.getItem(id).getEntity();
				filterData();
			}
		});
		HorizontalLayout areaLayout = new HorizontalLayout();
		areaLayout.setSizeFull();
		areaLayout.setMargin(true);
		areaLayout.addComponent(areaTree);
		areaLayout.setComponentAlignment(areaTree, Alignment.TOP_CENTER);

		tabSheet.addTab(typeTable, "意向", null);
		tabSheet.addTab(areaLayout, "区域", null);
		return tabSheet;
	}


	/**
	 * 设置按钮区域组件
	 */
	@Override
	protected void configureActions(HorizontalLayout actionArea) {
		//用户查询
		User cuser = UserUtil.getCurrentUser();
		if(!Duty.ZHI_YUAN.equals(cuser.getDuty().getName())){
			userBox = new ComboBox();
			JPAContainer<User> usercon = ContainerUtils.createJPAContainer(User.class);
			userBox.setContainerDataSource(usercon);
			userBox.setItemCaptionPropertyId("truename");
			if(Duty.BU_MENG_JING_LI.equals(cuser.getDuty().getName())){
				usercon.addContainerFilter(new Equal("department", cuser.getDepartment()));
				userBox.setValue(cuser.getId());
			}
			userBox.setWidth("75px");
			userBox.setInputPrompt("创建人");
			actionArea.addComponent(userBox);
			actionArea.setComponentAlignment(userBox, Alignment.MIDDLE_RIGHT);
		}
		//下次联系时间查询
		Label lab = new Label("联系时间");
		nextPlanA = new DateField();
		nextPlanA.setResolution(DateField.RESOLUTION_DAY);
		nextPlanA.setDateFormat("yyyy-MM-dd");
		nextPlanB = new DateField();
		nextPlanB.setDateFormat("yyyy-MM-dd");
		nextPlanB.setResolution(DateField.RESOLUTION_DAY);
		lab.setWidth("55px");
		nextPlanA.setWidth("85px");
		nextPlanB.setWidth("85px");
		actionArea.addComponent(lab);
		actionArea.addComponent(nextPlanA);
		Label split = new Label("－");
		split.setWidth("12px");
		actionArea.addComponent(nextPlanA);
		actionArea.addComponent(split);
		actionArea.addComponent(nextPlanB);
		actionArea.setComponentAlignment(lab, Alignment.MIDDLE_LEFT);
		actionArea.setComponentAlignment(split, Alignment.MIDDLE_LEFT);
		actionArea.setComponentAlignment(nextPlanB, Alignment.MIDDLE_LEFT);
		actionArea.setComponentAlignment(nextPlanA, Alignment.MIDDLE_LEFT);
		nextPlanA.addListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				searNextPanA = (Date) event.getProperty().getValue();
			}
		});
		nextPlanB.addListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				searNextPanB = (Date) event.getProperty().getValue();
			}
		});

		//查找框
		searchField = new TextField();
		searchField.setImmediate(true);
		searchField.setWidth("100%");
		searchField.setInputPrompt("客户名称|电话|地址|备注 回车确认");
		searchField.setNullRepresentation("");
		actionArea.addComponent(searchField);
		actionArea.setComponentAlignment(searchField, Alignment.MIDDLE_LEFT);
		actionArea.setExpandRatio(searchField, 1);

		//查找按钮
		Button searchb = new Button("搜索");
		searchb.setImmediate(true);
		actionArea.addComponent(searchb);
		searchb.addListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				searText = (String) searchField.getValue();
				filterData();
			}
		});
		searchb.setClickShortcut(KeyCode.ENTER);

		//全部显示按钮
		Button showAll = new Button("全部");
		actionArea.addComponent(showAll);
		showAll.addListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				searArea = null;
				searType = null;
				searNextPanA = null;
				searNextPanB = null;
				searText = null;
				typeTable.setValue(null);
				nextPlanA.setValue(null);
				nextPlanB.setValue(null);
				searchField.setValue(null);
				if(userBox != null){
					userBox.setValue(null);
				}
				filterData();
			}
		});
		//导入
		Button importbt = new Button("导入");
		importbt.setImmediate(true);
		actionArea.addComponent(importbt);
		final Window mainWin = (Window) SecurityUtils.getSubject().getSession().getAttribute("mainWindow");
		//TODO 导入
//		importbt.addListener(new ClickListener() {
//			@Override
//			public void buttonClick(ClickEvent event) {
//				UpLoadFileWindow upWin = new UpLoadFileWindow("数据导入");
//				mainWin.addWindow(upWin);
//			}
//		});

	}
	

	/**
	 * 公共查询过虑，在此控制数据权限
	 * @param filter
	 */
	@SuppressWarnings("unchecked")
	private void filterData(){
		JPAContainer<Customer> cusContainer = getTableContainer();

		User user = UserUtil.getCurrentUser();

		cusContainer.setApplyFiltersImmediately(false);
		cusContainer.removeAllContainerFilters();
		//设置职务权限
		Filter dutyFilter= getDutyFilter();
		//区域权限
		List<Area> myAreas = new Area().getAreasByUserId(user.getId());
		List<Equal> eqs = new ArrayList<Equal>();
		for(Area a: myAreas){
			eqs.add(new Equal("area", a));
		}
		Or areaFilter = new Or((Equal[]) eqs.toArray(new Equal[eqs.size()]));

		//将区域权限与职务权限or运算
		if(eqs.size() == 0 ){
			cusContainer.addContainerFilter(dutyFilter);
		}else{
			Or or = new Or(areaFilter,dutyFilter);
			cusContainer.addContainerFilter(or);
		}

		//设置定制权限
		Filter filter = getSearchFilter();
		if(filter != null){
			cusContainer.addContainerFilter(filter);
		}
		cusContainer.applyFilters();
	}


	private Filter getSearchFilter(){
		List<Filter> fs = new ArrayList<Filter>();

		if(this.searArea != null) fs.add(new Equal("area",searArea));
		if(this.searType != null) fs.add(new Equal("customerType",searType));
		if(userBox != null && userBox.getValue() != null){ 
			fs.add(new Equal("createBy.id", userBox.getValue()));
		}
		if(this.searNextPanA != null) {
			fs.add(new Compare.GreaterOrEqual("nextContactDate",generateSearchDate(searNextPanA, true)));
		}
		if(this.searNextPanB != null) {
			fs.add(new Compare.LessOrEqual("nextContactDate",generateSearchDate(searNextPanB, false)));
		}
		if(this.searText != null){
			Or or = new Or(
					new Like("name", "%"+searText+"%",false)
					,new Like("phone", "%"+searText+"%",false)
					,new Like("address", "%"+searText+"%",false)
					,new Like("remark", "%"+searText+"%",false)
					);
			fs.add(or);
		}
		if(fs.size() > 0){
			And and = new And((Filter[]) fs.toArray(new Filter[fs.size()]));
			return and;
		}else{
			return null;
		}

	}
	/**
	 * 生成查找用时间
	 * @param date
	 * @param beginDate 开始时间还是结束时间
	 */
	private Date generateSearchDate(Date date, boolean beginDate){
		if(date == null) return null;
		GregorianCalendar  calendar = new GregorianCalendar();
		calendar.setTime(date);
		if(beginDate){
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
		}else{
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
		}
		return calendar.getTime();
	}

	/**
	 * 格式化table的日期格式
	 */
	@Override
	protected Table createTable() {
		final Table table = new Table() {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			@Override
			protected String formatPropertyValue(Object rowId,
					Object colId, Property property) {
				// Format by property type
				if (property.getType() == Date.class) {
					Date date = (Date) property.getValue();
					String sv = date==null?"":sdf.format(date);
					return sv;
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		return table;
	}
	private Filter getDutyFilter(){
		User user = UserUtil.getCurrentUser();
		Set<User> myDepUsers = user.getDepartment().getUsers();
		//设置职务权限
		Filter dutyFilter;

		if(Duty.ZHI_YUAN.equals(user.getDuty().getName()) ){
			//职员只能看自己的
			dutyFilter = new Equal("createBy", user);
		}else if(Duty.BU_MENG_JING_LI.equals(user.getDuty().getName())){
			//部门经理只能查看本部门的
			List<Equal> eqs = new ArrayList<Equal>();
			for(User u: myDepUsers){
				eqs.add(new Equal("createBy", u));
			}
			Or or = new Or((Equal[]) eqs.toArray(new Equal[eqs.size()]));
			dutyFilter = or;
		}else{
			//其他人可以看到所有的
			dutyFilter = new Not(new Equal("id",null));
		}
		return dutyFilter;
	}
	@Override
	public void doSearch(){
		JPAContainer<Customer> cusContainer = getTableContainer();

		cusContainer.setApplyFiltersImmediately(false);
		cusContainer.removeAllContainerFilters();
		//设置职务权限
		Filter dutyFilter = getDutyFilter();
		cusContainer.addContainerFilter(dutyFilter);
		//设置搜索权限
		if(searchFilter != null){
			cusContainer.addContainerFilter(searchFilter);
		}
		cusContainer.applyFilters();
	}

	/**
	 * 设置反查filter
	 * @param filter
	 */
	public static void setSearchFilter(Filter filter){
		searchFilter = filter;
	}

}
