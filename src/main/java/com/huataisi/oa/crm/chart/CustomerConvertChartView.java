package com.huataisi.oa.crm.chart;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.huataisi.oa.crm.Area;
import com.huataisi.oa.crm.CusType;
import com.huataisi.oa.crm.Customer;
import com.huataisi.oa.crm.CustomerView;
import com.huataisi.oa.crm.Industry;
import com.huataisi.oa.domain.Duty;
import com.huataisi.oa.domain.User;
import com.huataisi.oa.security.UserUtil;
import com.huataisi.oa.util.ContainerUtils;
import com.vaadin.Application;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.filter.JoinFilter;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.Item;
import com.vaadin.data.util.filter.And;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.data.util.filter.IsNull;
import com.vaadin.data.util.filter.Or;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Select;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;

/**
 * 统计客户转化总量
 * @author 郝威
 *
 */
public class CustomerConvertChartView extends VerticalLayout implements View {
	
	Table table = new Table();
	DateField dateend = new DateField();
	List<CusType> cusTypeList = null;
	DateField datebegin = new DateField();
	Select seluser = new Select();
	Select selarea = new Select();
	Select selind = new Select();
	String whereCause  = "(";
	String userId = null;
	String areaId = null;
	String indId = null;
	String begin = null;
	String end = null;
	Date dbegin = null;
	Date dend = null;
	
	public CustomerConvertChartView() {
		table.addContainerProperty("customer", String.class, null);
		table.setColumnHeader("customer", "人员");
		table.addContainerProperty("area", String.class, null);
		table.setColumnHeader("area", "区域");
		table.addContainerProperty("industry", String.class, null);
		table.setColumnHeader("industry", "行业");
		table.addContainerProperty("date", String.class, null);
		table.setColumnHeader("date", "日期");
		cusTypeList = new CusType().findAll();
		for(CusType type : cusTypeList){
			table.addContainerProperty("count"+type.getId(), Integer.class, null);
			table.setColumnHeader("count"+type.getId(), type.getName());			
		}
		table.addContainerProperty("all", Integer.class, null);
		table.setColumnHeader("all", "小计");
		table.setSizeFull();
		table.setSelectable(true);
		table.setStyleName(Runo.TABLE_SMALL);
		
		HorizontalLayout actionArea = new HorizontalLayout();
        Label labbegin = new Label("开始时间：");
        labbegin.setWidth("70px");
        datebegin.setWidth("100px");
        datebegin.setResolution(DateField.RESOLUTION_DAY);
        datebegin.setDateFormat("yyyy-MM-dd");
        Label labend = new Label("结束时间：");
        labend.setWidth("70px");
        dateend.setDateFormat("yyyy-MM-dd");
        dateend.setResolution(DateField.RESOLUTION_DAY);
        dateend.setWidth("100px");
        Label labuser = new Label("人员：");
        labuser.setWidth("50px");
        Label labarea = new Label("区域：");
        labarea.setWidth("50px");
        Label labind = new Label("行业：");
        labind.setWidth("50px");
        
		JPAContainer<User> userContainer = ContainerUtils.createJPAContainer(User.class);
		User user = UserUtil.getCurrentUser();
		String duty = user.getDuty().getName();
		if(duty.equals(Duty.ZONG_JING_LI)){
			Or or = new Or(new Compare.Equal("department.id", 2),new Compare.Equal("department.id", 4));
			userContainer.addContainerFilter(or);						
		}else if(duty.equals(Duty.BU_MENG_JING_LI)){
			Filter userFilter = new Compare.Equal("department.id", user.getDepartment().getId());
			userContainer.addContainerFilter(userFilter);			
		}else if(duty.equals(Duty.ZHI_YUAN)){
			Filter userFilter = new Compare.Equal("id", user.getId());
			userContainer.addContainerFilter(userFilter);						
		}
		for(int i=0;i<userContainer.size();i++){
			whereCause += userContainer.getIdByIndex(i)+",";
		}
		whereCause += "-1)";
		seluser.setContainerDataSource(userContainer);
		seluser.setItemCaptionPropertyId("truename");
		seluser.setWidth("100px");
		
		JPAContainer<Area> areaContainer = ContainerUtils.createJPAContainer(Area.class);
		selarea.setContainerDataSource(areaContainer);
		selarea.setItemCaptionPropertyId("name");
		selarea.setWidth("100px");

		JPAContainer<Industry> indContainer = ContainerUtils.createJPAContainer(Industry.class);
		selind.setContainerDataSource(indContainer);
		selind.setItemCaptionPropertyId("name");
		selind.setWidth("100px");
		
		//统计按钮
		Button btnSearch = new Button("统计");
		actionArea.addComponent(labbegin);
		actionArea.addComponent(datebegin);
		actionArea.addComponent(labend);
		actionArea.addComponent(dateend);
		actionArea.addComponent(labuser);
		actionArea.addComponent(seluser);
		actionArea.addComponent(labarea);
		actionArea.addComponent(selarea);
		actionArea.addComponent(labind);
		actionArea.addComponent(selind);
		actionArea.addComponent(btnSearch);
		actionArea.setComponentAlignment(labbegin, Alignment.MIDDLE_RIGHT);
		actionArea.setComponentAlignment(labend, Alignment.MIDDLE_RIGHT);
		actionArea.setComponentAlignment(labuser, Alignment.MIDDLE_RIGHT);
		actionArea.setComponentAlignment(labarea, Alignment.MIDDLE_RIGHT);
		actionArea.setComponentAlignment(labind, Alignment.MIDDLE_RIGHT);
		actionArea.setComponentAlignment(datebegin, Alignment.MIDDLE_RIGHT);
		actionArea.setComponentAlignment(dateend, Alignment.MIDDLE_RIGHT);
		actionArea.setComponentAlignment(seluser, Alignment.MIDDLE_RIGHT);
		actionArea.setComponentAlignment(selarea, Alignment.MIDDLE_RIGHT);
		actionArea.setComponentAlignment(selind, Alignment.MIDDLE_RIGHT);
		actionArea.setComponentAlignment(btnSearch, Alignment.MIDDLE_RIGHT);
		
		btnSearch.setClickShortcut(KeyCode.ENTER);
		
		//整体布局
		this.setSizeFull();
		this.addComponent(actionArea);
		this.addComponent(table);
		this.setExpandRatio(table, 100);	
		
		Date date = new Date();
		GregorianCalendar  calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		datebegin.setValue(calendar.getTime());
		dateend.setValue(calendar.getTime());
		begin = calendar.get(Calendar.YEAR)+"-"+(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.DAY_OF_MONTH);
		dbegin = calendar.getTime();
		calendar.setTimeInMillis(calendar.getTimeInMillis()+86400000);
		end = calendar.get(Calendar.YEAR)+"-"+(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.DAY_OF_MONTH);
		dend = calendar.getTime();
		
		setChartData(cusTypeList, begin, end, null, null, null);
		
		btnSearch.addListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				userId = seluser.getValue() == null ? null : seluser.getValue().toString();
				areaId = selarea.getValue() == null ? null : selarea.getValue().toString();
				indId = selind.getValue() == null ? null : selind.getValue().toString();
				
				GregorianCalendar  calendar = new GregorianCalendar();
				if(datebegin.getValue() != null){
					Date d = (Date)datebegin.getValue();
					calendar.setTime(d);
					calendar.set(Calendar.HOUR_OF_DAY, 0);
					calendar.set(Calendar.MINUTE, 0);
					calendar.set(Calendar.SECOND, 0);
					begin = calendar.get(Calendar.YEAR)+"-"+(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.DAY_OF_MONTH);
					dbegin = calendar.getTime();
				}else{
					begin = null;
					dbegin = null;					
				}
				if(dateend.getValue() != null){
					Date d = (Date)dateend.getValue();
					calendar.setTime(d);
					calendar.set(Calendar.HOUR_OF_DAY, 0);
					calendar.set(Calendar.MINUTE, 0);
					calendar.set(Calendar.SECOND, 0);
					calendar.setTimeInMillis(calendar.getTimeInMillis()+86400000);
					end = calendar.get(Calendar.YEAR)+"-"+(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.DAY_OF_MONTH);
					dend = calendar.getTime();
				}else{
					end = null;
					dend = null;
				}
				table.removeAllItems();
				setChartData(cusTypeList, begin, end, userId, areaId, indId);
			}
		});
		
		addSearchEvent();
	}
	
	/**
	 * 反查事件
	 */
	private void addSearchEvent() {
		table.addListener(new ItemClickListener() {
			@Override
			public void itemClick(ItemClickEvent event) {
				String propId = String.valueOf(event.getPropertyId());
				if(propId.indexOf("count") != -1 || "noType".equals(propId) || "all".equals(propId)){//是否为汇总项
					Item item = event.getItem();
					int itemValue = Integer.valueOf(item.getItemProperty(propId).toString());
					if(itemValue > 0){//汇总数据不为0
						List<Filter> fs = new ArrayList<Filter>();
						List<Filter> fs2 = new ArrayList<Filter>();
						int size = table.getItemIds().size();
						if(size != Integer.valueOf(event.getItemId().toString())+1){//非合计条目
							String truename = String.valueOf(item.getItemProperty("customer"));
							String area = String.valueOf(item.getItemProperty("area"));
							String ind = String.valueOf(item.getItemProperty("industry"));
							fs.add(new And(new Compare.Equal("createBy.truename", truename)));
							fs2.add(new And(new Compare.Equal("createBy.truename", truename)));
							if(area != null){
								fs.add(new Compare.Equal("area.name", area));
								fs2.add(new Compare.Equal("area.name", area));
							}else{
								fs.add(new IsNull("area"));
								fs2.add(new IsNull("area"));
							}
							if(ind != null){
								fs.add(new Compare.Equal("industry.name", ind));
								fs2.add(new Compare.Equal("industry.name", ind));
							}else{
								fs.add(new IsNull("industry"));							
								fs2.add(new IsNull("industry"));							
							}
						}
						if(propId.indexOf("count") != -1){
							fs.add(new Compare.Equal("customerType.id", propId.substring(5)));							
						}
						if(userId != null){
							fs.add(new Compare.Equal("createBy.id", userId));
							fs2.add(new Compare.Equal("createBy.id", userId));
						}
						if(areaId != null){
							fs.add(new Compare.Equal("area.id", areaId));
							fs2.add(new Compare.Equal("area.id", areaId));
						}
						if(indId != null){
							fs.add(new Compare.Equal("industry.id", indId));
							fs2.add(new Compare.Equal("industry.id", indId));
						}
						if(begin != null){
							fs.add(new JoinFilter("typeTrack", new Compare.GreaterOrEqual("changeDate", dbegin)));
							fs2.add(new JoinFilter("typeTrack", new Compare.GreaterOrEqual("changeDate", dbegin)));
						}
						if(end != null){
							fs.add(new JoinFilter("typeTrack", new Compare.Less("changeDate", dend)));
							fs2.add(new JoinFilter("typeTrack", new Compare.Less("changeDate", dend)));
						}
						
						And and = new And((Filter[]) fs.toArray(new Filter[fs.size()]));
						JPAContainer<Customer> cusContainer = ContainerUtils.createJPAContainer(Customer.class);
						cusContainer.addContainerFilter(and);
						if(cusContainer.size() == 0){
							and = new And((Filter[]) fs2.toArray(new Filter[fs2.size()]));
							cusContainer.removeAllContainerFilters();
							cusContainer.addContainerFilter(and);
						}
						fs.clear();
						for(int i=0;i<cusContainer.size();i++){
							fs.add(new Compare.Equal("id", cusContainer.getIdByIndex(i)));
						}
						Or or = new Or((Filter[]) fs.toArray(new Filter[fs.size()]));
						CustomerView.setSearchFilter(or);
						UserUtil.navigateTo("customer/search");					
					}
				}
			}
		});
	}
	
	
	/**
	 * 填充数据
	 * @param cusTypeList
	 * @param beginDate
	 * @param endDate
	 * @param userId
	 * @param areaId
	 * @param indId
	 */
	private void setChartData(List<CusType> cusTypeList, String beginDate, String endDate, String userId, String areaId, String indId){
		List<Object[]> list = new Customer().getCustomerConvertChart(beginDate, endDate, userId, areaId, indId, whereCause);
		Object[] countItem = new Object[table.getColumnHeaders().length];
		for(int i = 0; i < list.size(); i++){
			Object[] item = list.get(i);
			table.addItem(item, i);
			for(int m = 4; m < item.length; m++){
				if(countItem[m] == null){
					countItem[m] = 0L;
				}
				countItem[m] = (Long)countItem[m]+(Long)item[m];
			}
		}
		if(list != null && list.size() > 0){
			countItem[0] = "合计";
			table.addItem(countItem, list.size());			
		}
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

}
