package com.huataisi.oa.crm;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.huataisi.oa.util.Caption;
import com.huataisi.oa.util.IdEntity;

/**
 * 按意向对客户进行分类
 * @author Administrator
 *
 */
@Entity
@Table(name="OA_CRM_customerType")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CusType extends IdEntity<CusType>{
	private static final long serialVersionUID = 1L;
	public CusType() {
	}
	public CusType(String name) {
		this.name = name;
	}

	@Caption("分类名称")
	@NotNull
	private String name;
	
	@Override
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public CusType findCusTypeByName(String cusTypeName){
			
		List<CusType> cusTypes = entityManager()
				.createQuery("select c from CusType c where c.name = ? ",CusType.class)
				.setParameter(1, cusTypeName).getResultList();
		if (cusTypes == null || cusTypes.size() == 0) {
			return null;
		}else {
			return cusTypes.get(0);
		}
	}
	
	
	
}
