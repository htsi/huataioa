package com.huataisi.oa.crm;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.huataisi.oa.util.Caption;
import com.huataisi.oa.util.IdEntity;

/**
 * 客户类型变化历史记录
 */
@Entity
@Table(name="OA_CRM_CTypeHistory")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CusTypeHistory extends IdEntity<CusTypeHistory>{
	private static final long serialVersionUID = 1L;

	public CusTypeHistory(CusType ctype, Date date) {
		this.ctype = ctype;
		this.changeDate = date;
	}
	public CusTypeHistory() {
	}
	

	@Caption("目标分类")
	@NotNull
	@ManyToOne
	private CusType ctype;
	
	@Caption("转换时间")
	private Date changeDate;
	
	@ManyToOne()
	@JoinColumn(name="customer_id")  
	private Customer customer;
	
	@Override
	public String toString() {
		return ctype.getName();
	}

	public CusType getCtype() {
		return ctype;
	}

	public void setCtype(CusType ctype) {
		this.ctype = ctype;
	}

	public Date getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
}
