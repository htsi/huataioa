package com.huataisi.oa.crm;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.huataisi.oa.util.Caption;
import com.huataisi.oa.util.IdEntity;

@Entity
@Table(name="OA_CRM_TRACK")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Track extends IdEntity<Track>{
	private static final long serialVersionUID = 1L;

	public Track(Date ttime, String remark) {
		this.ttime = ttime;
		this.remark = remark;
	}
	public Track(){
		ttime = new Date();
	}
	
	@Caption("\u65F6\u95F4")
	private Date ttime = new Date();
	
	@Caption("联系情况")
	@Column(length=2000)
	private String remark;
	
	@Caption("下次任务")
	@Column(length=2000)
	private String nextPlan;
	
	@ManyToOne()
	@JoinColumn(name="customer_id")  
	private Customer customer;

	public Date getTtime() {
		return ttime;
	}
	public void setTtime(Date ttime) {
		this.ttime = ttime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getNextPlan() {
		return nextPlan;
	}
	public void setNextPlan(String nextPlan) {
		this.nextPlan = nextPlan;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
}
