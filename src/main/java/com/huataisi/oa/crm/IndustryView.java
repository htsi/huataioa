package com.huataisi.oa.crm;

import com.huataisi.oa.util.AutoEntityView;

@SuppressWarnings("serial")
public class IndustryView extends AutoEntityView<com.huataisi.oa.crm.Industry> {
	public IndustryView() {
		super(Industry.class);
	}
}
