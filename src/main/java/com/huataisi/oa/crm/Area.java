package com.huataisi.oa.crm;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.huataisi.oa.domain.User;
import com.huataisi.oa.util.Caption;
import com.huataisi.oa.util.IdEntity;

@Entity
@Table(name="OA_CRM_AREA")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Area extends IdEntity<Area>{
	private static final long serialVersionUID = 1L;

	public Area(String name, Area parent) {
		this.name = name;
		this.parent = parent;
	}

	public Area(String name, Area parent, boolean isLeaf) {
		this.name = name;
		this.parent = parent;
		this.leaf = isLeaf;
	}
	public Area(){
		//不知道为什么，只有在此处new 一下新建时才不会报错
		users = new LinkedHashSet<User>();
	}

	@NotNull
	@Caption("\u5730\u533A\u540D\u79F0")
	private String name;
	
	@Caption("\u4E0A\u7EA7")
	@ManyToOne
	private Area parent;
	
	
	@Caption("\u6700\u5E95\u5C42\uFF1F")
	private boolean leaf;

	@Override
	public String toString() {
		return name;
	}
	
	@Caption("\u53EF\u7EF4\u62A4\u4EBA\u5458")
	@ManyToMany(fetch=FetchType.EAGER)
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private Set<User> users = new LinkedHashSet<User>();
	
	public  List<Area> getAreasByUserId(Object userId){
		return entityManager()
				.createQuery("select a from Area a join a.users u where u .id = ?", Area.class)
				.setParameter(1, userId).getResultList();
	}
	
	public  Area getAreaByName(String areaName){
			
			List<Area> areas = entityManager()
					.createQuery("select a from Area a where a.name = ? ",Area.class)
					.setParameter(1, areaName).getResultList();
			if (areas == null || areas.size() == 0) {
				return null;
			}else {
				return areas.get(0);
			}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Area getParent() {
		return parent;
	}

	public void setParent(Area parent) {
		this.parent = parent;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	
	
}
