package com.huataisi.oa.crm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.huataisi.oa.domain.User;
import com.huataisi.oa.security.UserUtil;
import com.huataisi.oa.util.AutoEntityForm;
import com.huataisi.oa.util.ContainerUtils;
import com.huataisi.oa.util.LayoutField;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Item;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;


@SuppressWarnings("serial")
public class CustomerFormA extends AutoEntityForm<Customer> {

	public CustomerFormA() {
		super(Customer.class);
		
		ArrayList<Object> baseFields = new ArrayList<Object>(Arrays.asList(
			new LayoutField("name", 2,1),"nextContactDate","customerType"
			,new LayoutField("remark", 3,2)
			,new LayoutField("phone", 4,1)
			,new LayoutField("url", 4,1)
			,new LayoutField("address", 3,1)
			,"area","scale","industry","useSoft","category","lastTrackDate","createDate"));
		User user = UserUtil.getCurrentUser();
		//硬件部不显示这些字段
		if("硬件部".equals(user.getDepartment().getName())){
			baseFields.remove("scale");
			baseFields.remove("industry");
			baseFields.remove("useSoft");
		}
		//业务部不显示这些字段
		if("销售部".equals(user.getDepartment().getName())){
			baseFields.remove("category");
		}
		
		//添加最终字段到form上
		GridLayout layout = new GridLayout(7,4);
		layout.setSizeFull();
		addTabVisibleFields("基本信息",layout,baseFields.toArray());
		addTabVisibleFields("跟踪记录",null,"tracks");
		addTabVisibleFields("联系人",null,"contacts");
		addTabVisibleFields("转换记录",null,"typeTrack");

		getField("lastTrackDate").setReadOnly(true);
		getField("createDate").setReadOnly(true);
		getField("typeTrack").setReadOnly(true);
		Field sf = 	getField("scale");
		if(sf != null){
			((TextField)sf).setInputPrompt("请输入大概人数");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void beforeCommit(Item item) {
		Field tracksField = getField("tracks");
		//更新最近跟踪时间
		if(tracksField.isModified()){
			List<Track> tacks = (List<Track>) tracksField.getValue();
			if(tacks != null && tacks.size() > 0){
				Date date =  tacks.get(0).getTtime();
				for(Track t : tacks){
					if(t.getTtime() != null && (date == null || t.getTtime().after(date))) {
						date = t.getTtime();
					}
				}
				item.getItemProperty("lastTrackDate").setValue(date);
			}
		}
		//设置创建人
		if(item.getItemProperty("createBy").getValue() == null){
			User user = UserUtil.getCurrentUser();
			item.getItemProperty("createBy").setValue(user);
		}

		Field customerTypeField = getField("customerType");
		//设置类型变化历史
		if(customerTypeField.isModified()){
			//获得选择的ID
			Object cusTypeId = customerTypeField.getValue();
			if(cusTypeId != null){
				//获得选择的CusType
				JPAContainer<CusType> con = ContainerUtils.createJPAContainer(CusType.class);
				CusType type= con.getItem(cusTypeId).getEntity();

				//设置历史
				EntityItem<Customer> eitem = (EntityItem<Customer>) item;//当前主对象
				CusTypeHistory ctHis = new CusTypeHistory(type,new Date());//创建历史对象
				ctHis.setCustomer(eitem.getEntity());//关联主对象
				ContainerUtils.createJPAContainer(CusTypeHistory.class).addEntity(ctHis);//存储
			}
		}

	}

}
