package com.huataisi.oa.security;

import java.util.Vector;

import org.springframework.transaction.annotation.Transactional;

import com.huataisi.oa.domain.User;
import com.huataisi.oa.util.ContainerUtils;
import com.huataisi.oa.util.EntityForm;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.AbstractValidator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.Window;

public class EditUserWindow extends Window {
	private static final long serialVersionUID = 1L;
	private User currentUser = UserUtil.getCurrentUser();
	private EntityForm<User> userForm = new EntityForm<User>(User.class);
	private Form pwForm = new Form();
	private String oldPW = currentUser.getPassword() == null?"":currentUser.getPassword();
	private PasswordField newPasswordField; 
	private PasswordField oldPasswordField; 
	public EditUserWindow(String caption){
		super(caption);

		this.setHeight("550px");
		this.setWidth("400px");
		this.setModal(true);
		this.center();
		userForm.setImmediate(true);
		userForm.setItemDataSource(new BeanItem<User>(currentUser));
		Vector<String> vec = new Vector<String>();
		vec.add("username");
		vec.add("truename");
		vec.add("department");
		vec.add("duty");
		vec.add("email");
		vec.add("phone");
		vec.add("mobile");
		vec.add("qq");
		userForm.setVisibleItemProperties(vec);
		userForm.getField("username").setReadOnly(true);
		userForm.getField("truename").setReadOnly(true);
		userForm.getField("department").setReadOnly(true);
		Validator validator= userForm.getField("department").getValidators().iterator().next();
		userForm.getField("department").removeValidator(validator);
		userForm.getField("duty").setReadOnly(true);
		//设置控件的宽度
		userForm.getField("email").setWidth("200px");
		userForm.getField("phone").setWidth("200px");
		userForm.getField("mobile").setWidth("200px");
		userForm.getField("qq").setWidth("200px");
		
		//原密码
		oldPasswordField = new PasswordField("原密码");
		oldPasswordField.setWidth("185px");
		pwForm.addField("oldPassword", oldPasswordField);
		//新密码
		newPasswordField = new PasswordField("新密码");
		newPasswordField.setWidth("185px");
		pwForm.addField("newPassword", newPasswordField);
		//新密码
		final PasswordField confirmPasswordField = new PasswordField("确认密码");
		confirmPasswordField.setWidth("185px");
		pwForm.addField("confimPassword", confirmPasswordField);


		//按钮布局
		final HorizontalLayout buttonLayout = new HorizontalLayout();
		Button saveButton = new Button("保存");
		Button cancelButton = new Button("取消");
		buttonLayout.addComponent(saveButton);
		buttonLayout.addComponent(cancelButton);
		buttonLayout.setStyleName("oa-personInfoBtnLayout");


		Label readMe = new Label("原密码和新密码都不填写\n\t表示对密码不进行修改");
		readMe.setContentMode(Label.CONTENT_TEXT);
		readMe.setStyleName("oa-personInfoLinkReadMe");

		HorizontalLayout personInfoPanel = new HorizontalLayout();
		personInfoPanel.addComponent(userForm);
		personInfoPanel.addComponent(pwForm);
		//TODO .....
//		this.addComponent(personInfoPanel);
//
//		//	this.addComponent(personInfoForm);
//		this.addComponent(readMe);
//		this.addComponent(buttonLayout);

		cancelButton.addListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				setVisible(false);
			}
		});

		saveButton.addListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				doCommit();
				setVisible(false);
			}
		});

		oldPasswordField.addValidator(new Validator() {
			public boolean isValid(Object value) {
				if(newPasswordField.getValue() == null){
					return false;
				}
				if(oldPW.equals(value)){
					return true;
				}else{
					return false;
				}
			}

			@Override
			public void validate(Object value) throws InvalidValueException {
				if(!isValid(value)){
					if(newPasswordField.getValue() == null){
						throw new InvalidValueException("新密码不能为空");
					}else{
						throw new InvalidValueException("旧密码错误");
					}
				}

			}
		});
		newPasswordField.addValidator(new Validator() {
			public boolean isValid(Object value) {
				if(!oldPW.equals(oldPasswordField.getValue())) return false;
				if(value.equals(confirmPasswordField.getValue())){
					return true;
				}
				if(oldPasswordField.getValue() == null){
					return false;
				}
				return false;
			}

			@Override
			public void validate(Object value) throws InvalidValueException {
				if(!isValid(value)){
					if(!oldPW.equals(oldPasswordField.getValue())) {
						throw new InvalidValueException("修改密码时必须提供旧密码");
					}else{
						throw new InvalidValueException("两次输入密码不一致");
					}
				}

			}

		});
	}
	@Transactional
	private void doCommit(){
		userForm.commit();
		pwForm.commit();
		EntityItem<User> item = ContainerUtils.createJPAContainer(User.class)
				.getItem(currentUser.getId());
		item.getItemProperty("mobile").setValue(currentUser.getMobile());
		item.getItemProperty("qq").setValue(currentUser.getQq());
		item.getItemProperty("email").setValue(currentUser.getEmail());
		item.getItemProperty("phone").setValue(currentUser.getPhone());
		item.getItemProperty("password").setValue(newPasswordField.getValue());
	}


}
