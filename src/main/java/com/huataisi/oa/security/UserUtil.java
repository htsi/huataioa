package com.huataisi.oa.security;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

import com.huataisi.oa.domain.User;
import com.vaadin.navigator.Navigator;

public class UserUtil {
	//主视图在SESSION中的名称
	public static final String APP_MAIN_NAVIGATOR = "APP_MAIN_NAVIGATOR";
	
	/**
	 * 返回当前登录用户，且该用户为受控Entity
	 * @return
	 */
	public static User getCurrentUser(){
		Object id =  SecurityUtils.getSubject().getPrincipal();
		User u = new User().findOne((String) id);
		return u;
		
	}
	
	/**
	 * 导航到指定视图
	 * @param viewUri
	 */
	public static void navigateTo(String viewUri){
		Session session = SecurityUtils.getSubject().getSession();
		Navigator mv = (Navigator) session.getAttribute(APP_MAIN_NAVIGATOR);
		mv.navigateTo(viewUri);
	}
}
