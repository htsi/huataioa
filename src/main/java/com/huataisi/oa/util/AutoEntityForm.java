package com.huataisi.oa.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import javax.persistence.metamodel.PluralAttribute;
import javax.persistence.metamodel.Type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;
import org.vaadin.addon.customfield.ConvertingValidator;
import org.vaadin.addon.customfield.PropertyConverter;
import org.vaadin.addon.customfield.beanfield.BeanFieldPropertyConverter;
import org.vaadin.addon.customfield.beanfield.BeanSetFieldPropertyConverter;

import com.huataisi.oa.web.EntityEditor;
import com.vaadin.addon.beanvalidation.BeanValidationValidator;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.AbstractComponentContainer;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Select;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;

@SuppressWarnings("serial")
@Configurable
public class AutoEntityForm<E extends IdEntity<E>> extends CustomComponent implements EntityEditor {
	Logger logger = LoggerFactory.getLogger(AutoEntityForm.class);

	private AbsoluteLayout mainLayout;

	private Panel scrollPanel;

	private VerticalLayout scrollContent;

	private HorizontalLayout buttonLayout;

	private Button deleteButton;

	private Button cancelButton;

	private Button saveButton;

	private Label errorMessageLabel;

	private AbstractComponentContainer fieldLayout;

	// data item being edited
	private Item item;
	private Class<E> entityClass;

	private Map<Object, LayoutField> LayoutFieldMap = new LinkedHashMap<Object, LayoutField>();
	private Map<Object, PropertyConverter> converterMap = new LinkedHashMap<Object, PropertyConverter>();
	private Map<Object,java.lang.reflect.Field>  fieldTypeMap = new LinkedHashMap<Object, java.lang.reflect.Field>();
	private List<TabField> visibleTabFields = new ArrayList<TabField>();

	private TabSheet tabSheet;
	private GridLayout gridLayout ;

	public AutoEntityForm(Class<E> entityClass) {
		this(entityClass, null);
	}
	public AutoEntityForm(Class<E> entityClass, List<TabField> setVisibleFields) {
		if(setVisibleFields != null){
			this.visibleTabFields = setVisibleFields;
		}
		this.entityClass = entityClass;
		buildMainLayout();
		setCompositionRoot(mainLayout);
		configure();
	}


	public void addSaveActionListener(ClickListener listener) {
		saveButton.addListener(listener);
	}

	public void addCancelActionListener(ClickListener listener) {
		cancelButton.addListener(listener);
	}

	public void addDeleteActionListener(ClickListener listener) {
		deleteButton.addListener(listener);
	}

	public void setSaveAllowed(boolean canSave) {
		saveButton.setVisible(canSave);
		cancelButton.setVisible(canSave);
		saveButton.setEnabled(canSave);
		cancelButton.setEnabled(canSave);

		// do not change the enabled state of the delete button
		fieldLayout.setEnabled(canSave);
	}

	public void setDeleteAllowed(boolean canDelete) {
		deleteButton.setVisible(canDelete);
		deleteButton.setEnabled(canDelete);
	}

	public void setCommitErrorMessage(String message) {
		errorMessageLabel.setVisible(message != null);
		errorMessageLabel.setValue(message);
	}

	public void commit() {
		Item item = getItemDataSource();
		if (item != null) {
			beforeCommit(item);
			validateFields();
			setCommitErrorMessage(null);
			commitFields();
			afterCommit(item);
		}
	}
	/**
	 * 提交前回调方法，一般可以在此处处理业务。
	 * 传入的item不会为null,不用做null判断
	 * @param item
	 */
	public void beforeCommit(Item item){

	}
	public void afterCommit(Item item){

	}

	public void setItemDataSource(Item item) {
		// TODO implement

		this.item = item;

		setFieldValues(item);
		setCommitErrorMessage(null);
	}

	public Item getItemDataSource() {
		return item;
	}

	@Override
	public void focus() {
		Field field = getFirstField();
		if (field != null) {
			field.focus();
		}
	}


	private AbsoluteLayout buildMainLayout() {
		// common part: create layout
		mainLayout = new AbsoluteLayout();

		// top-level component properties
		setWidth("100.0%");
		setHeight("100.0%");

		// scrollPanel
		scrollPanel = buildScrollPanel();
		mainLayout.addComponent(scrollPanel);

		return mainLayout;
	}


	private Panel buildScrollPanel() {
		// common part: create layout
		scrollPanel = new Panel();
		scrollPanel.setWidth("100.0%");
		scrollPanel.setHeight("100.0%");
		scrollPanel.setImmediate(false);

		// scrollContent
		scrollContent = buildScrollContent();
		scrollPanel.setContent(scrollContent);

		return scrollPanel;
	}


	private VerticalLayout buildScrollContent() {
		// common part: create layout
		scrollContent = new VerticalLayout();
		scrollContent.setWidth("100.0%");
		scrollContent.setImmediate(false);
		scrollContent.setMargin(false);
		scrollContent.setSpacing(true);

		// fieldLayout
		fieldLayout = buildFieldLayout();
		scrollContent.addComponent(fieldLayout);

		// errorMessageLabel
		errorMessageLabel = new Label();
		errorMessageLabel.setStyleName("errormessage");
		errorMessageLabel.setValue("");
		errorMessageLabel.setImmediate(false);
		scrollContent.addComponent(errorMessageLabel);

		// buttonLayout
		buttonLayout = buildButtonLayout();
		scrollContent.addComponent(buttonLayout);

		return scrollContent;
	}


	@SuppressWarnings({ "unchecked", "rawtypes"})
	private AbstractComponentContainer buildFieldLayout() {
		//生成字段
		rebuildLayoutFields();

		//添加字段到tabsheet上
		tabSheet = new TabSheet();
		tabSheet.setStyleName(Runo.TABSHEET_SMALL);
		if(visibleTabFields.size() == 0){//如果没有设置可见字段，使用默认值
			addFieldToTab(new TabField("详细信息", gridLayout, new ArrayList(LayoutFieldMap.keySet())));
		}
		for(TabField tf : visibleTabFields){
			addFieldToTab(tf);
		}

		tabSheet.addListener(new SelectedTabChangeListener() {
			@Override
			public void selectedTabChange(SelectedTabChangeEvent event) {
				GridLayout gl = (GridLayout) event.getTabSheet().getSelectedTab();
				Iterator<Component> it = gl.getComponentIterator();
				while(it.hasNext()){
					Layout layout = (Layout) it.next();
					Component c = layout.getComponentIterator().next();
					if(c instanceof EntitySubTable){
						EntitySubTable stable = ((EntitySubTable)c);
						if(!stable.isLoadDateImmediate()){
							stable.loadData();
							stable.setLoadDateImmediate(true);
						}
					}
				}
			}
		});
		return tabSheet;
	}

	/**
	 * 添加字段到标签页，同时应用字段的合并单元格设置
	 * @param tf
	 * @param tabSheet
	 */
	private void addFieldToTab(TabField tf){
		//构建GridLayout
		GridLayout gl = tf.gridLayout;
		if(gl == null){
			gl = new GridLayout();
			int size = tf.fields.size();
			int coluCount = size%4 == 0 ? size/4 : size/4+1;//按默认4行计算列数
			if(coluCount > 1)  gl.setColumns(coluCount);
			//如果只有一个字段且是子表，设置100%
			if(size == 1){
				LayoutField layoutField = LayoutFieldMap.get(tf.fields.get(0).toString());
				if(layoutField.getField() instanceof EntitySubTable){
					gl.setSizeFull();
				}
			}
			tf.gridLayout = gl;
		}
		gl.removeAllComponents();
		tabSheet.addTab(gl,tf.caption);

		//将fields添加到GridLayout上
		for(Object userField : tf.fields){
			LayoutField layoutField = LayoutFieldMap.get(userField.toString());
			if(layoutField == null){
				logger.error("字段"+userField+"不存在于"+entityClass.getName()+"中，不能做为field添加到form中");
				continue;
			}
			Field field = layoutField.getField();
			VerticalLayout cl = new VerticalLayout();
			cl.setWidth("100%");
			cl.addComponent(field);
			//TODO 需要完善布局
//			MarginInfo mi = new MarginInfo(true);
//			mi.setMargins(new VMarginInfo(5));
//			cl.setMargin(mi);
//			cl.setMargin(false, true, false, false);
			if(layoutField.getColSpan() == 1 && layoutField.getRowSpan()==1){
				gl.addComponent(cl);
			}else{
				gl.addComponent(cl);
				GridLayout.Area area = gl.getComponentArea(cl);
				int xFrom = area.getColumn1();
				int xTo = xFrom + layoutField.getColSpan()-1;
				int yFrom = area.getRow1();
				int yTo = yFrom + layoutField.getRowSpan()-1;
				gl.removeComponent(cl);
				gl.addComponent(cl,xFrom,yFrom,xTo,yTo);
			}
		}
	}
	private void rebuildLayoutFields(){
		LayoutFieldMap.clear();
		fieldTypeMap.clear();
		//生成字段
		java.lang.reflect.Field[] fields = entityClass.getDeclaredFields();
		for(java.lang.reflect.Field f : fields){
			LayoutField layoutField = getLayoutField(f);
			if(layoutField == null) continue;
			LayoutFieldMap.put(f.getName(), layoutField);
			fieldTypeMap.put(f.getName(), f);
		}
	}

	private LayoutField getLayoutField(java.lang.reflect.Field f){
		String propertyId = f.getName();
		//如果用户设置了要显示的字段，则优先使用
		if(visibleTabFields.size() > 0){
			for(TabField tabField : visibleTabFields){
				for(Object field : tabField.fields){//循环每一个字段进行匹配
					//LayoutField类型，如果是没设置field,重建
					if(field instanceof LayoutField){
						LayoutField lf = ((LayoutField) field);
						if(lf.getFieldName().equals(propertyId)){
							if(lf.getField() == null){
								lf.setField(getFieldByType(f));
							}
							return lf;
						}
					}
					//字符串类型的字段，直接重建
					else if(field instanceof String && field.equals(propertyId)){
						return new LayoutField(propertyId,getFieldByType(f));
					}
				}
			}
		}
		//如果用户没用设置要显示的字段，默认使用设置了caption的字段
		else{
			Caption caption = f.getAnnotation(Caption.class);
			if(caption != null){
				return new LayoutField(f.getName(),getFieldByType(f));
			}
		}
		return null;
	}
	private Field getFieldByType(java.lang.reflect.Field f){
		Caption caption = f.getAnnotation(Caption.class);
		Field field;
		Column column = f.getAnnotation(Column.class);
		if(f.getAnnotation(ManyToMany.class) != null) {
			TwinColSelect twinColSelect = new TwinColSelect();
			twinColSelect.setImmediate(true);
			field = twinColSelect;
		}else if(f.getAnnotation(ManyToOne.class) != null){
			ComboBox comboBox = new ComboBox();
			comboBox.setImmediate(true);
			field = comboBox;
		}else if(f.getAnnotation(OneToMany.class) != null){
			String mapedBy = f.getAnnotation(OneToMany.class).mappedBy();
			Class<?> c  = detectReferencedType(f.getName());
			EntitySubTable<?> sub = new EntitySubTable(c,mapedBy,caption != null ? caption.value() : null);
			sub.setImmediate(true);
			field = sub;
		}else if(boolean.class.isAssignableFrom(f.getType()) ||
				Boolean.class.isAssignableFrom(f.getType())) {
			CheckBox cb = new CheckBox();
			field = cb;
		}else if(Enum.class.isAssignableFrom(f.getType())) {
			Select select = new Select();
			select.setContainerDataSource(ContainerUtils.createEnumContainer((Class<? extends Enum<?>>) f.getType()));
			field = select;
		}else if(Date.class.isAssignableFrom(f.getType())) {
			DateField df = new DateField();
			df.setResolution(DateField.RESOLUTION_MIN);
			df.setDateFormat("yyyy-MM-dd HH:mm");
			field = df;
		}else if(column != null && column.length() >= 2000) {
			TextArea ta = new TextArea();
			ta.setNullRepresentation("");
			ta.setImmediate(true);
			field = ta;
		}else {
			TextField textField= new TextField();
			textField.setNullRepresentation("");
			textField.setImmediate(true);
			field = textField;
		}
		return field;
	}


	private HorizontalLayout buildButtonLayout() {
		// common part: create layout
		buttonLayout = new HorizontalLayout();
		buttonLayout.setImmediate(false);
		buttonLayout.setMargin(false);
		buttonLayout.setSpacing(true);

		// saveButton
		saveButton = new Button();
		saveButton.setCaption("保存");
		saveButton.setStyleName("primary");
		saveButton.setImmediate(true);
		buttonLayout.addComponent(saveButton);

		// cancelButton
		cancelButton = new Button();
		cancelButton.setCaption("取消");
		cancelButton.setImmediate(true);
		buttonLayout.addComponent(cancelButton);

		// deleteButton
		deleteButton = new Button();
		deleteButton.setCaption("删除");
		deleteButton.setImmediate(true);
		buttonLayout.addComponent(deleteButton);
		buttonLayout.setComponentAlignment(deleteButton, new Alignment(48));

		return buttonLayout;
	}

	@Override
	public String getProcessDefinitionKey() {
		return null;
	}

	public Field getField(Object propertyId) {
		LayoutField lf = LayoutFieldMap.get(propertyId);
		return lf == null ? null : lf.getField();
	}

	private void configure() {
		configureFields();
		configureContainersForFields();
		configureConverters();
		configureValidators();
	}

	public void refresh() {
		configureContainersForFields();
		configureConverters();
		configureValidators();
	}

	public boolean isModified() {
		if (getItemDataSource() != null) {
			for (Object propertyId : getItemDataSource().getItemPropertyIds()) {
				Field field = getField(propertyId);
				if (field != null && field.isModified()) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * 设置每个已经生成字段的标题，长宽，WriteThrough，InvalidAllowed属性
	 */
	private void configureFields() {
		for (Object propertyId : LayoutFieldMap.keySet()) {
			Field field = getField(propertyId);
			if (field == null) {
				continue;
			}
			if (field instanceof TextField) {
				((TextField) field).setNullRepresentation("");
			}
			//默认子表不显示标题，lazy load
			java.lang.reflect.Field f = fieldTypeMap.get(propertyId);
			Caption caption = f.getAnnotation(Caption.class);
			if(field instanceof EntitySubTable){
				((EntitySubTable<?>)field).setLoadDateImmediate(false);
			}else{
				field.setCaption(caption != null ? caption.value() : DefaultFieldFactory.createCaptionByPropertyId(f.getName()));
			}
			field.setWidth("100%");
			field.setInvalidAllowed(true);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void configureContainersForFields() {
		for(Object propertyId : LayoutFieldMap.keySet()){
			Field field = getField(propertyId);
			java.lang.reflect.Field f = fieldTypeMap.get(propertyId);
			if (f.getAnnotation(ManyToMany.class) != null) {
				Class c  = detectReferencedType(propertyId);
				Container con = ContainerUtils.createJPAContainer(c);
				((AbstractSelect) field).setContainerDataSource(con);
				((AbstractSelect) field).setItemCaptionMode(AbstractSelect.ITEM_CAPTION_MODE_ITEM);
			}
			else if (f.getAnnotation(ManyToOne.class) != null) {
				Class c = f.getType();
				Container con = ContainerUtils.createJPAContainer(c);
				((AbstractSelect) field).setContainerDataSource(con);
				((AbstractSelect) field).setItemCaptionMode(AbstractSelect.ITEM_CAPTION_MODE_ITEM);
			}
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void configureConverters() {
		for(Object propertyId : LayoutFieldMap.keySet()){
			Field field = getField(propertyId);
			java.lang.reflect.Field f = fieldTypeMap.get(propertyId);
			if (f.getAnnotation(ManyToMany.class) != null ) {
				Class c  = detectReferencedType(propertyId);
				Container container = ((AbstractSelect) field).getContainerDataSource();
				PropertyConverter converter = new BeanSetFieldPropertyConverter<IdEntity<?>,String>(c, container, "id");
				converterMap.put(propertyId, converter);
			}
			else if (f.getAnnotation(ManyToOne.class) != null ) {
				Class c = f.getDeclaringClass();
				Container container = ((AbstractSelect) field).getContainerDataSource();
				PropertyConverter converter = new BeanFieldPropertyConverter(c, container, "id");
				converterMap.put(propertyId, converter);
			}
		}
	}

	private void configureValidators() {
		for (Object propertyId : LayoutFieldMap.keySet()) {
			Field field = getField(propertyId);
			if (field != null) {
				Collection<Validator> validators = field.getValidators();
				if (validators != null) {
					for (Validator validator : new ArrayList<Validator>(field.getValidators())) {
						if (validator instanceof BeanValidationValidator || validator instanceof ConvertingValidator) {
							field.removeValidator(validator);
						}
					}
				}
				BeanValidationValidator validator = new BeanValidationValidator(getEntityClass(), String.valueOf(propertyId));
				if (validator.isRequired()) {
					field.setRequired(true);
					field.setRequiredError(validator.getRequiredMessage());
				}
				PropertyConverter converter = getConverter(propertyId);
				if (converter == null) {
					field.addValidator(validator);
				} else {
					field.addValidator(new ConvertingValidator(validator, converter));
				}
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private PropertyConverter getConverter(Object propertyId) {
		return converterMap.get(propertyId);
	}


	private void validateFields() {
		if (getItemDataSource() != null) {
			for (Object propertyId : getItemDataSource().getItemPropertyIds()) {
				Field field = getField(propertyId);
				if (field != null && !field.isReadOnly()) {
					field.validate();
				}
			}
		}
	}

	private void commitFields() {
		if (getItemDataSource() != null) {
			for (Object propertyId : getItemDataSource().getItemPropertyIds()) {
				Field field = getField(propertyId);
				if (field != null && !field.isReadOnly()) {
					field.commit();
				}
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private void setFieldPropertyDataSource(Object propertyId, Property property) {
		Field field = getField(propertyId);
		if (field == null) {
			return;
		}
		if(field instanceof EntitySubTable){
			((EntitySubTable)field).setPropertyDataSource(getItemDataSource(), property);
		}
		else if (property == null) {
			field.setPropertyDataSource(null);
		} else {
			PropertyConverter converter = getConverter(propertyId);
			if (converter != null) {
				converter.setPropertyDataSource(property);
				field.setPropertyDataSource(converter);
			} else {
				if (field instanceof CheckBox && property.getValue() == null) {
					property.setValue(Boolean.FALSE);
				}
				field.setPropertyDataSource(property);
			}
		}
	}

	private void setFieldValues(Item item) {
		if (item != null) {
			// set values for fields in item
			for (Object propertyId : item.getItemPropertyIds()) {
				setFieldPropertyDataSource(propertyId, item.getItemProperty(propertyId));
			}
			// other fields are not touched by default
		} else {
			// reset all fields
			for (Object propertyId : LayoutFieldMap.keySet()) {
				setFieldPropertyDataSource(propertyId, null);
			}
		}
	}

	private Field getFirstField() {
		Iterator<Object> it = LayoutFieldMap.keySet().iterator();
		if (it.hasNext()) {
			return getField(it.next());
		}
		return null;
	}

	private Class<E> getEntityClass() {
		return this.entityClass;
	}

	/**
	 * Detects the type entities in "collection types" (oneToMany, ManyToMany).
	 * 
	 * @param propertyId
	 * @param masterEntityClass
	 * @return the type of entities in collection type
	 */
	private Class detectReferencedType(Object propertyId) {
		EntityManagerFactory emf = null;
		try {
			emf = getEntityClass().newInstance().entityManager().getEntityManagerFactory();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Class referencedType = null;
		Metamodel metamodel = emf.getMetamodel();
		Set<EntityType<?>> entities = metamodel.getEntities();
		for (EntityType<?> entityType : entities) {
			Class<?> javaType = entityType.getJavaType();
			if (javaType == entityClass) {
				Attribute<?, ?> attribute = entityType.getAttribute(propertyId.toString());
				PluralAttribute pAttribute = (PluralAttribute) attribute;
				Type elementType = pAttribute.getElementType();
				referencedType = elementType.getJavaType();
				break;
			}
		}
		return referencedType;
	}


	/**
	 * 设置form每个标签页的要显示的字段
	 * @param tabCaption
	 * @param tabGridLayout
	 * @param fields 要显示的字段，分为两种类型:<br>
	 * 		<li>1.String.对应Entity中的字段名,系统会根据字段类型自动生成Field</li>
	 * 		<li>2.LayoutField.其中包含了字段可以占多少单元格信息。
	 * 			如果只设置fieldName,系统会自动生成Field，如果同时设置了Field,系统会使用设置的Field</li>
	 */
	public void addTabVisibleFields(String tabCaption,GridLayout tabGridLayout,Object... fields){
		for(TabField tf :visibleTabFields){
			if(tf.caption.equals(tabCaption)) throw new RuntimeException("标签"+tabCaption+"已经存在，禁止添加");
		}

		TabField  tf = new TabField(tabCaption, tabGridLayout, Arrays.asList(fields));
		visibleTabFields.add(tf);

		// scrollContent
		scrollPanel.setContent(null);
		scrollContent = buildScrollContent();
		scrollPanel.setContent(scrollContent);

		configure();
		requestRepaint();
	}
	public void setGridLayout(GridLayout gridLayout){
		this.gridLayout = gridLayout;
		// scrollContent
		scrollPanel.setContent(null);
		scrollContent = buildScrollContent();
		scrollPanel.setContent(scrollContent);

		configure();
		requestRepaint();
	}

	private class TabField {
		public String caption;
		public GridLayout gridLayout;
		public List<Object> fields = new ArrayList<Object>();
		public TabField(String caption, GridLayout gridLayout, List<Object> fields) {
			this.caption = caption;
			this.gridLayout = gridLayout;
			this.fields = fields;
		}
	}

	
}
