package com.huataisi.oa.util;

import java.util.Date;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.huataisi.oa.addressbook.AddressBookView;
import com.huataisi.oa.crm.Area;
import com.huataisi.oa.crm.AreaView;
import com.huataisi.oa.crm.CusType;
import com.huataisi.oa.crm.CusTypeView;
import com.huataisi.oa.crm.Customer;
import com.huataisi.oa.crm.CustomerView;
import com.huataisi.oa.crm.Industry;
import com.huataisi.oa.crm.IndustryView;
import com.huataisi.oa.crm.chart.CustomerChartView;
import com.huataisi.oa.crm.chart.CustomerConvertChartView;
import com.huataisi.oa.crm.chart.CustomerTelChartView;
import com.huataisi.oa.domain.Department;
import com.huataisi.oa.domain.Duty;
import com.huataisi.oa.domain.Menu;
import com.huataisi.oa.domain.MenuType;
import com.huataisi.oa.domain.Role;
import com.huataisi.oa.domain.User;
import com.huataisi.oa.leave.LeaveView;
import com.huataisi.oa.salary.SalaryView;
import com.huataisi.oa.web.ui.DepartmentView;
import com.huataisi.oa.web.ui.DutyView;
import com.huataisi.oa.web.ui.MenuView;
import com.huataisi.oa.web.ui.RoleView;
import com.huataisi.oa.web.ui.UserView;
import com.huataisi.oa.worklog.WorkLogView;

/**
 * 初始化数据库类
 * @author Administrator
 */
@SuppressWarnings("rawtypes")
@Component//注释后该初始化动作就不会执行
public class InitData implements ApplicationListener {

	@Override
	@Transactional
	public void onApplicationEvent(ApplicationEvent arg0) {
		long count = new Menu().count();
		if(count > 0) return;
		//通讯录菜单
		Menu maddressBook = new Menu("通讯录",MenuType.APPLICATION,null,"addressBook",AddressBookView.class.getName());
		maddressBook.persist();

		//考勤菜单
		Menu mcq = new Menu("考勤管理",MenuType.MODULE,null,null,null);
		mcq.setIcon("img/folder24x24.png");
		Menu mleave = new Menu("外出请假",MenuType.APPLICATION,mcq,"leave",LeaveView.class.getName());
		mcq.persist();
		mleave.persist();
		//客户关系管理菜单
		Menu mcrm = new Menu("客户关系",MenuType.MODULE,null,null,null);
		mcrm.setIcon("img/folder24x24.png");
		Menu mcustomer = new Menu("客户管理",MenuType.APPLICATION,mcrm,"customer",CustomerView.class.getName());
		Menu marea = new Menu("区域管理",MenuType.APPLICATION,mcrm,"area",AreaView.class.getName());
		Menu mimp = new Menu("分类管理",MenuType.APPLICATION,mcrm,"importance",CusTypeView.class.getName());
		Menu mindustry = new Menu("行业管理",MenuType.APPLICATION,mcrm,"industry",IndustryView.class.getName());
		Menu mcuschart = new Menu("客户总量统计",MenuType.APPLICATION,mcrm,"mcuschart",CustomerChartView.class.getName());
		Menu mcustelchart = new Menu("电话总量统计",MenuType.APPLICATION,mcrm,"mcustelchart",CustomerTelChartView.class.getName());
		Menu mcusconvertchart = new Menu("转化客户统计",MenuType.APPLICATION,mcrm,"mcusconvertchart",CustomerConvertChartView.class.getName());
		mcrm.persist();
		mcustomer.persist();
		marea.persist();
		mimp.persist();
		mindustry.persist();
		mcuschart.persist();
		mcustelchart.persist();
		mcusconvertchart.persist();

		//权限管理菜单
		Menu quanxian = new Menu("权限管理",MenuType.MODULE,null,null,null);
		quanxian.setIcon("img/folder24x24.png");
		quanxian.persist();
		Menu menu = new Menu("系统菜单",MenuType.APPLICATION,quanxian,"menu",MenuView.class.getName());
		menu.persist();
		Menu role = new Menu("角色管理",MenuType.APPLICATION,quanxian,"role",RoleView.class.getName());
		role.persist();
		//组织机构菜单
		Menu org = new Menu("组织机构",MenuType.MODULE,null,null,null);
		org.setIcon("img/folder24x24.png");
		org.persist();
		Menu dep = new Menu("部门管理",MenuType.APPLICATION,org,"deparment",DepartmentView.class.getName());
		dep.persist();
		Menu person = new Menu("人员管理",MenuType.APPLICATION,org,"user",UserView.class.getName());
		person.persist();
		Menu duty = new Menu("职务管理",MenuType.APPLICATION,org,"duty",DutyView.class.getName());
		duty.persist();

		//工作日志
		Menu mrz = new Menu("日志管理",MenuType.MODULE,null,null,null);
		mrz.setIcon("img/folder24x24.png");
		Menu mlogs = new Menu("工作日志",MenuType.APPLICATION,mrz,"worklogs",WorkLogView.class.getName());
		mrz.persist();
		mlogs.persist();
		//工资
		Menu mgzgl = new Menu("工资管理",MenuType.MODULE,null,null,null);
		mgzgl.setIcon("img/folder24x24.png");
		mgzgl.persist();
		Menu gz = new Menu("工资",MenuType.APPLICATION,mgzgl,"salary",SalaryView.class.getName());
		gz.persist();


		//权限数据
		Role radmin = new Role(Role.ADMIN,"系统管理员组");
		radmin.addMenu(menu);
		radmin.addMenu(role);
		radmin.addMenu(dep);
		radmin.addMenu(person);
		radmin.addMenu(duty);
		radmin.persist();
		Role ruser = new Role(Role.USER,"一般用户组");
		ruser.addMenu(mcustomer);
		ruser.addMenu(mleave);
		ruser.addMenu(maddressBook);
		ruser.addMenu(mlogs);
		ruser.addMenu(gz);
		ruser.addMenu(mcuschart);
		ruser.addMenu(mcustelchart);
		ruser.addMenu(mcusconvertchart);
		ruser.persist();
		Role ryewu = new Role("yewuAdmin","业务管理员");
		ryewu.addMenu(marea);
		ryewu.addMenu(mimp);
		ryewu.addMenu(mindustry);
		ryewu.persist();

		//部门数据
		Department ht = new Department("华泰软件",null);
		ht.persist();
		Department hard = new Department("硬件部",ht);
		hard.persist();
		Department dev = new Department("软件部",ht);
		dev.persist();
		Department sale = new Department("销售部",ht);
		sale.persist();
		Department houqin = new Department("后勤部",ht);
		houqin.persist();
		//职务数据
		Duty zongjingli = new Duty(Duty.ZONG_JING_LI);
		zongjingli.persist();
		Duty depJingLi = new Duty(Duty.BU_MENG_JING_LI);
		depJingLi.persist();
		Duty yuanGong = new Duty(Duty.ZHI_YUAN);
		yuanGong.persist();
		Duty dadmin = new Duty(Duty.ADMIN);
		dadmin.persist();

		//user
		User usyb = new User("syb","","刘延宾",ht,zongjingli,"3560475@qq.com","132130762",9560475,"沈总","0371-67681931");
		usyb.addRole(ruser);
		usyb.addRole(ryewu);
		usyb.persist();	

		User urll = new User("rll","","任丽",houqin,depJingLi,"9d453788@qq.com","137839522",94453788,"任经理","0371-63659873");
		urll.addRole(ruser);
		urll.persist();

		User ulfb = new User("lfb","","刘波",sale,depJingLi,"18397870@qq.com","186397870",6240546,"刘经理","0371-55551479  13383715391");
		ulfb.addRole(ruser);
		ulfb.addRole(ryewu);
		ulfb.persist();

		User uwxb = new User("wxb","","魏宝",sale,yuanGong,"7512347@qq.com","15995396",75213747,null,"0371-60964348");
		uwxb.addRole(ruser);
		uwxb.persist();

		User uzl = new User("zl","","张磊",sale,yuanGong,"2723513@qq.com","158091201",27231693,null,"13383715390  0371-55551489");
		uzl.addRole(ruser);
		uzl.persist();

		User uwfl = new User("wfl","","王龙",hard,depJingLi,"1124013411@qq.com","138389046",1124013411,"王经理","13343847380  0371-60964348");
		uwfl.addRole(ruser);
		uwfl.addRole(ryewu);
		uwfl.persist();

		User ullq = new User("llq","","刘奇",hard,yuanGong,"418258115@qq.com","13783434895",418258115,null,"13343847380  0371-60964348");
		ullq.addRole(ruser);
		ullq.persist();

		User ufwt = new User("fwt","","丰涛",hard,yuanGong,"303269767@qq.com","18638599979",303269767,null,"13343847380  0371-60964348");
		ufwt.addRole(ruser);
		ufwt.persist();

		User uc = new User("chl","","崔亮",dev,depJingLi,"67892238@qq.com","13663835604",67892238,"崔经理","0371-63659872");
		uc.addRole(ruser);
		uc.addRole(ryewu);
		uc.persist();

		User uhw = new User("hw","","郝威",dev,yuanGong,"59498498@qq.com","18736035837",594973849,null,"0371-63659872");
		uhw.addRole(ruser);
		uhw.persist();

		User ushw = new User("shw","","宋望",dev,yuanGong,"songnglulove@vip.qq.com","15939007445",529173628,null,"0371-63659872");
		ushw.addRole(ruser);
		ushw.persist();

		User uhjx = new User("hjx","","候嘉鑫",dev,yuanGong,"aries0415@vip.qq.com","15903627200",270040511,null,"0371-63659872");
		uhjx.addRole(ruser);
		uhjx.persist();


		User uadmin = new User("admin","","管理员",ht,dadmin,"679238@qq.com","1313130789",67892238,null,"");
		uadmin.addRole(radmin);
		uadmin.addRole(ryewu);
		uadmin.addRole(ruser);
		uadmin.persist();


		//客户关系数据
		Area az = new Area("郑州市",null);
		az.persist();
		Area ag = new Area("巩义",az,true);
		ag.persist();
		Area ax = new Area("荥阳",az,true);
		ax.persist();
		Area axm = new Area("新密",az,true);
		axm.persist();
		Area ak = new Area("开封市",null);
		ak.persist();
		CusType ia = new CusType("非常重要");
		CusType ib = new CusType("比较重要");
		CusType ic = new CusType("不太重要");
		CusType id = new CusType("很不重要");
		ia.persist();
		ib.persist();
		ic.persist();
		id.persist();
		Industry inDut = new Industry("教育");
		Industry inGov = new Industry("政府");
		Industry inProdu = new Industry("医辽");
		Industry intongxin = new Industry("通信");
		inDut.persist();
		inGov.persist();
		inProdu.persist();
		intongxin.persist();

				Customer c1 = new Customer("河南移动郑州分公司",5000,intongxin,az,ia,uc,new Date());
				c1.persist();
		
				Customer c2 = new Customer("中国石化河南分公司",3000,intongxin,ax,ib,uc,new Date());
				c2.persist();
		
				CusType[] ctype = new CusType[]{ia,ia,ib,ib,ic,ic,id,id};
				Integer[] sacles = new Integer[]{1000,500,30000,10000,5500};
				Industry[] indus = new Industry[]{inDut,inGov,inProdu,intongxin,inProdu};
				User[] users = new User[]{uc,ulfb,uwxb,uzl,uwfl,ullq,ufwt,ushw,uhjx};
				Area[] areas = new Area[]{az,ag,ax,axm,ak};
				for(int i = 1; i < 1000; i++){
					Customer cc = new Customer("某某公司"+i
								,sacles[i%sacles.length]
								,indus[i%indus.length],areas[i%areas.length]
								,ctype[i%ctype.length]
								,users[i%users.length],new Date());
					cc.persist();
				}
	}

}
