package com.huataisi.oa.util;

import java.lang.reflect.ParameterizedType;

@SuppressWarnings("serial")
public abstract class HierarchialEntity<E> extends IdEntity<E> implements
		HierarchialEntityInterface<E> {
	
	@SuppressWarnings("unchecked")
	@Override
	public Class<E> getEntityClass(){
		if(entityClass == null){
			return (Class<E>) ((ParameterizedType) getClass()
					.getGenericSuperclass()).getActualTypeArguments()[0];
		}else{
			return entityClass;
		}
	}
}
