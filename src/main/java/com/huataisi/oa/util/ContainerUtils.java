package com.huataisi.oa.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.huataisi.oa.web.EntityProviderUtil;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Container;
import com.vaadin.data.util.IndexedContainer;

public class ContainerUtils {

    public static String CAPTION_PROPERTY_NAME = "caption";
    
    public static Container createContainerFromMap(Map<?, String> hashMap) {
        IndexedContainer container = new IndexedContainer();
        container.addContainerProperty(CAPTION_PROPERTY_NAME, String.class, "");
        
        Iterator<?> iter = hashMap.keySet().iterator();
        while(iter.hasNext()) {
            Object itemId = iter.next();
            container.addItem(itemId);
            container.getItem(itemId).getItemProperty(CAPTION_PROPERTY_NAME).setValue(hashMap.get(itemId));
        }
        
        return container;
    }
    
    public static Container createEnumContainer(Class<? extends Enum<?>> enumClass) {
        LinkedHashMap<Enum<?>, String> enumMap = new LinkedHashMap<Enum<?>, String>();
        for (Object enumConstant : enumClass.getEnumConstants()) {
            enumMap.put((Enum<?>) enumConstant, enumConstant.toString());
        }
        return createContainerFromMap(enumMap);
    }
    
    
    public static <E extends IdEntity<E>>  JPAContainer<E> createJPAContainer(Class<E> c) {
		JPAContainer<E> container = new JPAContainer<E>(c);
		container.setEntityProvider(new EntityProviderUtil().getEntityProvider(c));
		return container;
	}
    
    public static <E extends IdEntity<E>>  JPAContainer<E> createJPABatchableContainer(Class<E> c) {
    	JPAContainer<E> container = new JPAContainer<E>(c);
    	container.setEntityProvider(new EntityProviderUtil().getBatchableEntityProvider(c));
    	return container;
    }
    
    public static <E extends HierarchialEntity<E>> JPAContainer<E> createJPAHierarchialContainer(Class<E> c) {
    	class HJPAContainer<H> extends JPAContainer<E>{
    		private static final long serialVersionUID = 1L;

			public HJPAContainer(Class<E> entityClass) {
				super(entityClass);
				setEntityProvider(new EntityProviderUtil().getEntityProvider(entityClass));
				setParentProperty("parent");
			}

			@Override
			public boolean areChildrenAllowed(Object itemId) {
				return super.areChildrenAllowed(itemId)
		                && !getItem(itemId).getEntity().isLeaf();
			}
    	   
    	 }
    	return new HJPAContainer<E>(c);
    }
       
}