package com.huataisi.oa.util;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.Version;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.beans.factory.annotation.Configurable;
@SuppressWarnings("serial")
@MappedSuperclass
@Configurable
public abstract class IdEntity<E> implements Serializable{
	@PersistenceContext
	@Transient
	private  EntityManager entityManager;
	@Transient
	protected Class<E> entityClass;  
	
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid.hex")
	protected String id;
	
	@Version
	private Integer version;
	
	public EntityManager entityManager() {
		EntityManager em = entityManager;
		if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em; 
	}
	
	@SuppressWarnings("unchecked")
	public Class<E> getEntityClass(){
		if(entityClass == null){
			return (Class<E>) ((ParameterizedType) getClass()
					.getGenericSuperclass()).getActualTypeArguments()[0];
		}else{
			return entityClass;
		}
	}
	
	@SuppressWarnings("unchecked")
	public E persist(){
		 entityManager().persist(this);
		 return (E)this; 
	}
	public void remove(){
		E e = findOne(id);
		delete(e);
	}
	
	public List<E> findAll() {
		CriteriaBuilder cb = entityManager().getCriteriaBuilder();
		CriteriaQuery<E> c = cb.createQuery(getEntityClass());
		c.from(getEntityClass());
		TypedQuery<E> q = entityManager().createQuery(c); 
		return q.getResultList();
	}

	public void flush() {
		entityManager().flush();
	}
	
	public long count() {
		CriteriaBuilder cb = entityManager().getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		cq.select(cb.count(cq.from(getEntityClass())));

		return entityManager().createQuery(cq).getSingleResult();
	}
	
	public E findOne(Serializable id){
		return (E)  entityManager().find(getEntityClass(), id);
	}
	public void delete(E e) {
		 entityManager().remove(e);
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	} 

	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
}
