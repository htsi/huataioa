package com.huataisi.oa.util;
import java.util.Collection;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.Select;
import com.vaadin.ui.TableFieldFactory;
import com.vaadin.ui.TextField;
@SuppressWarnings("serial")
public class EntityFieldFactory implements FormFieldFactory, TableFieldFactory{
	final Logger logger = LoggerFactory.getLogger(EntityFieldFactory.class);

	@Override
	public Field createField(Item item, Object propertyId, Component uiContext) {
		Class<?> type = item.getItemProperty(propertyId).getType();
		Field field = createFieldByPropertyType(type);
		field.setCaption(createCaptionByPropertyId(propertyId));
		return field;
	}
	@Override
	public Field createField(Container container, Object itemId, Object propertyId, Component uiContext) {
		Property containerProperty = container.getContainerProperty(itemId,propertyId);
		Class<?> type = containerProperty.getType();
		
		Field field = createFieldByPropertyType(type);
		field.setCaption(createCaptionByPropertyId(propertyId));
		return field;
	}


	public static Field createFieldByPropertyType(Class<?> type) {
		Field field;
		// Null typed properties can not be edited
		if (type == null) {
			return null;
		}

		// Item field
		else if (Item.class.isAssignableFrom(type)) {
			field = new Form();
		}

		// Date field
		else if (Date.class.isAssignableFrom(type)) {
			final DateField df = new DateField();
			df.setResolution(DateField.RESOLUTION_MIN);
			df.setDateFormat("yyyy-MM-dd HH:mm");
			field = df;
		}

		// Boolean field
		else if (Boolean.class.isAssignableFrom(type)) {
			field = new CheckBox();
		}
		// 集合类型 field
//		else if (Collection.class.isAssignableFrom(type)) {
//			//TODU
//			return new EntitySubTable<E>(type, name);
//		}
		// Enum field
		else if (Enum.class.isAssignableFrom(type)) {
			Select select = new Select();
			select.setContainerDataSource(ContainerUtils.createEnumContainer((Class<? extends Enum<?>>) type));
			return select;
		}
		else{
			TextField tf = new TextField();
			tf.setNullRepresentation("");
			field = tf;
		}
		

		return field;
	}
	
	
	/**
	 * If name follows method naming conventions, convert the name to spaced
	 * upper case text. For example, convert "firstName" to "First Name"
	 * 
	 * @param propertyId
	 * @return the formatted caption string
	 */
	public static String createCaptionByPropertyId(Object propertyId) {
		String name = propertyId.toString();
		if (name.length() > 0) {

			if (name.indexOf(' ') < 0
					&& name.charAt(0) == Character.toLowerCase(name.charAt(0))
					&& name.charAt(0) != Character.toUpperCase(name.charAt(0))) {
				StringBuffer out = new StringBuffer();
				out.append(Character.toUpperCase(name.charAt(0)));
				int i = 1;

				while (i < name.length()) {
					int j = i;
					for (; j < name.length(); j++) {
						char c = name.charAt(j);
						if (Character.toLowerCase(c) != c
								&& Character.toUpperCase(c) == c) {
							break;
						}
					}
					if (j == name.length()) {
						out.append(name.substring(i));
					} else {
						out.append(name.substring(i, j));
						out.append(" " + name.charAt(j));
					}
					i = j + 1;
				}

				name = out.toString();
			}
		}
		return name;
	}
	

}
