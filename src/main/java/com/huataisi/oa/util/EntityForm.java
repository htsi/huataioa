package com.huataisi.oa.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.vaadin.addon.customfield.PropertyConverter;
import org.vaadin.addon.customfield.beanfield.BeanFieldPropertyConverter;
import org.vaadin.addon.customfield.beanfield.BeanSetFieldPropertyConverter;

import com.huataisi.oa.crm.Area;
import com.huataisi.oa.domain.User;
import com.vaadin.addon.beanvalidation.BeanValidationValidator;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.TwinColSelect;

/**
 * Vaadin {@link Form} using the JSR-303 (javax.validation) declarative bean
 * validation on its fields.
 * 
 * Most validation is performed using {@link BeanValidationValidator}. In
 * addition, fields are automatically marked as required when necessary based on
 * the {@link NotNull} annotations.
 * 
 * If the item is a {@link BeanItem}, the exact type of the bean is used in
 * setting up validators. Otherwise, validation will only be performed based on
 * beanClass, and behavior is undefined for subclasses that have fields not
 * present in the superclass.
 * 
 * @author Petri Hakala
 * @author Henri Sara
 */
public class EntityForm<T> extends Form {
	private static final long serialVersionUID = 1L;

	private Class<T> beanClass;
	private Locale locale;
	private Map<String,String> captions = new HashMap<String, String>();

	/**
	 * Creates a form that performs validation on beans of the type given by
	 * beanClass.
	 * 
	 * Full validation of sub-types of beanClass is performed if the item used
	 * is a {@link BeanItem}. Otherwise, the class given to this constructor
	 * determines which properties are validated.
	 * 
	 * @param beanClass
	 *            base class of beans for the form
	 */
	public EntityForm(Class<T> beanClass) {
		if (beanClass == null) {
			throw new IllegalArgumentException("Bean class cannot be null");
		}
		this.beanClass = beanClass;

		//设置标题，只有加了caption
		for(java.lang.reflect.Field field : beanClass.getDeclaredFields()){
			Caption caption = field.getAnnotation(Caption.class);
			if(caption != null){
				captions.put(field.getName(), caption.value());
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void addField(Object propertyId, Field field) {
		Item item = getItemDataSource();
		field.setPropertyDataSource(item.getItemProperty(propertyId));
		Class<? extends T> beanClass = this.beanClass;
		if (item instanceof BeanItem) {
			beanClass = (Class<? extends T>) ((BeanItem<T>) item).getBean()
					.getClass();
		}
		//只编辑本类声明的字段
		try {
			java.lang.reflect.Field f =beanClass.getDeclaredField((String) propertyId);
			if(Collection.class.isAssignableFrom(f.getType())){
				BeanValidationValidator validator = BeanValidationValidator
						.addValidator(field, propertyId, beanClass);
				if (locale != null) {
					validator.setLocale(locale);
				}
			}
		} catch (SecurityException e) {
		} catch (NoSuchFieldException e) {
			return;//没有该字段很有可能是get方法开头，不是字段，跳过
		}

		//设置标题
		if(captions.containsKey(propertyId)){
			field.setCaption(captions.get(propertyId));
		}

		super.addField(propertyId, field);
	}

	/**
	 * Sets the item data source for the form. If the new data source is a
	 * {@link BeanItem}, its bean must be assignable to the bean class of the
	 * form.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void setItemDataSource(Item newDataSource) {
		if ((newDataSource instanceof BeanItem)
				&& !beanClass.isAssignableFrom(((BeanItem) newDataSource)
						.getBean()
						.getClass())) {
			throw new IllegalArgumentException("Bean must be of type "
					+ beanClass.getName());
		}
		super.setItemDataSource(newDataSource);
	}

	/**
	 * Sets the item data source for the form. If the new data source is a
	 * {@link BeanItem}, its bean must be assignable to the bean class of the
	 * form.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void setItemDataSource(Item newDataSource, Collection propertyIds) {
		if ((newDataSource instanceof BeanItem)
				&& !beanClass.isAssignableFrom(((BeanItem) newDataSource)
						.getBean()
						.getClass())) {
			throw new IllegalArgumentException("Bean must be of type "
					+ beanClass.getName());
		}
		super.setItemDataSource(newDataSource, propertyIds);
		
	}

	/**
	 * Returns the base class of beans supported by this form.
	 * 
	 * @return bean class
	 */
	public Class<T> getBeanClass() {
		return beanClass;
	}

	/**
	 * Sets the locale to be used for validation messages.
	 * 
	 * This only applies to fields created after setting the locale (i.e. after
	 * the next {@link #setItemDataSource()} call), not to the current content
	 * of the form.
	 */
	@Override
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	@Override
	public Locale getLocale() {
		return locale;
	}

	@Override
	protected void bindPropertyToField(Object propertyId, Property property, Field field) {
		if(field instanceof TwinColSelect){
			Container container = ((TwinColSelect)field).getContainerDataSource();
			PropertyConverter converter = new BeanSetFieldPropertyConverter<User, String>(User.class, container, "id");
			converter.setPropertyDataSource(property);
			((TwinColSelect)field).setPropertyDataSource(converter);
		}else {
			super.bindPropertyToField(propertyId, property, field);
        }

	}


}
