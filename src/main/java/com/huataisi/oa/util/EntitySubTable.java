package com.huataisi.oa.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vaadin.addon.beanvalidation.BeanValidationValidator;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.terminal.Paintable.RepaintRequestEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Field;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;


/**
 * 加了caption的字段默认会可见
 * @author 
 *
 * @param <E>
 */
@SuppressWarnings("serial")
public class EntitySubTable<E extends IdEntity<E>> extends VerticalLayout implements Field {
	private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private final String deleteCheckBoxID = "deleteCheckBox" + System.currentTimeMillis();
	private JPAContainer<E>  container;

	private Table table;
	private Button addButton;
	private Button editButton;
	private Class<E> entityClass;
	private Set<Field> fields = new HashSet<Field>();
	private String mainEntityFieldname;//子表实体中关联主表的字段名
	private Set<Object> removed = new HashSet<Object>();//要删除的记录ID值集合
	private boolean edited = false;//是否编辑了数据，不包含删除
	private List<Object> visibleColumns = new ArrayList<Object>();
	private boolean readOnly = false;
	private Object mainEntityInstance;//主对象实例
	private Item item;
	private boolean loadDateImmediate = true;
	/**
	 * @param propertyClass 子表的class
	 * @param mainEntityFieldname 子表中主表字段的名称
	 * @param buttonName
	 */
	public EntitySubTable(final Class<E> propertyClass, final String mainEntityFieldname, String buttonName) {
		this.entityClass = propertyClass;
		this.mainEntityFieldname = mainEntityFieldname;
		table = new Table(){
			@Override
			protected String formatPropertyValue(Object rowId, Object colId, Property property) {
				if (property.getType() == Date.class) {
					Date date = (Date) property.getValue();
					return date==null?"":sdf.format(date);
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		table.setStyleName(Runo.TABLE_SMALL);
		table.setEditable(false);
		table.setSelectable(true);
		table.setSizeFull();
		table.setRowHeaderMode(Table.ROW_HEADER_MODE_INDEX);
		table.setPageLength(4);

		//init buttons
		CssLayout cl = new CssLayout();
		if(buttonName == null) buttonName = "";
		editButton = new Button("编辑"+buttonName);
		addButton = new Button("增加"+buttonName);
		cl.addComponent(editButton);
		cl.addComponent(addButton);
		addButton.addListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				try {
					table.setEditable(true);
					Object id = container.addEntity(entityClass.newInstance());
					EntityItem<?> ei = container.getItem(id);
					ei.getItemProperty(mainEntityFieldname).setValue(mainEntityInstance);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		editButton.addListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				table.setEditable(true);
			}
		});

		//add components
		this.addComponent(table);
		this.addComponent(cl);
		this.setExpandRatio(table, 1);
		this.setSizeFull();
		
	}

	@Override
	public void setPropertyDataSource(Property newDataSource) {
		throw new UnsupportedOperationException();
	}

	/**
	 * 设置所属item,用于过滤table数据，使其为主表的子表
	 * @param item
	 */
	public void setPropertyDataSource(Item item,Property newDataSource){
		this.item = item;
		removed.clear();
		edited = false;
		if(loadDateImmediate){
			loadData();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void loadData(){
		//配制table,因为该操作要连接数据库所以不在构造函数中配制，推迟到设置表数据时执行以提高效率
		if(container == null){
			configTable();
		}
		EntityItem<E> i = (EntityItem<E>)item;
		
		//设置数据
		if(i != null ){
			this.mainEntityInstance = i.getEntity();
			container.setApplyFiltersImmediately(false);
			if(table.isEditable()) table.setEditable(false);
			container.removeContainerFilters(mainEntityFieldname);
			container.addContainerFilter(new Compare.Equal(mainEntityFieldname, i.getEntity()));
			container.applyFilters();
		}
	}
	
	@Override
	public void commit() throws SourceException, InvalidValueException {
		if(isModified()) {
			for(Object itemId : removed){
				container.removeItem(itemId);
			}
			container.commit();
		}
	}

	/**
	 * 配制table的列，字段，删除checkBox
	 */
	private void configTable(){
		container = ContainerUtils.createJPABatchableContainer(entityClass);
		container.setAutoCommit(false);
		table.setContainerDataSource(container);

		table.setTableFieldFactory(new EntityFieldFactory() {
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				Field field = super.createField(container, itemId, propertyId, uiContext);
				BeanValidationValidator.addValidator(field, propertyId, entityClass);
				field.setWidth("100%");
				field.addListener(new ValueChangeListener() {
					@Override
					public void valueChange( com.vaadin.data.Property.ValueChangeEvent event) {
						edited = true;
					}
				});
				fields.add(field);
				return field;
			}
		});
		
		//增加删除checkbox,按钮并绑定删除事件
		if(readOnly){
			addButton.setVisible(false);
			editButton.setVisible(false);
			addButton.setEnabled(false);
			editButton.setEnabled(false);
		}else{
			table.addGeneratedColumn(deleteCheckBoxID, new ColumnGenerator() {
				@Override
				public Object generateCell(Table source, final Object itemId, Object columnId) {
					final CheckBox cb = new CheckBox();
					cb.setImmediate(true);
					cb.addListener(new ValueChangeListener() {
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							if(cb.booleanValue()) removed.add(itemId);
							else removed.remove(itemId);
						}
					});
					return cb;
				}
			});
			visibleColumns.add(deleteCheckBoxID);
			table.setColumnHeader(deleteCheckBoxID, "删除");
			table.setColumnWidth(deleteCheckBoxID, 30);
		}
		
		//设置默认可见列及每列标题
		for(java.lang.reflect.Field field : entityClass.getDeclaredFields()){
			Caption caption = field.getAnnotation(Caption.class);
			if(caption != null){
				visibleColumns.add(field.getName());
				table.setColumnHeader(field.getName(), caption.value());
			}
		}
		table.setVisibleColumns(visibleColumns.toArray());
	}

	@Override
	public List<E>  getValue() {
		List<E> es = new ArrayList<E>();
		Collection<Object> ids = container.getItemIds();
		for(Object id : ids){
			es.add(container.getItem(id).getEntity());
		}
		return es;
	}
	public Table getTable(){
		return table;
	}
	public Object getSelected(){
		return table.getValue();
	}

	@Override
	public void validate() throws InvalidValueException {
		for(Field field : fields){
			field.validate();
		}
	}
	/**
	 * 设置是否只读，目前只能在table未添加数据前
	 * (即调用setPropertyDataSource(Item item,Property newDataSource)设置，
	 */
	@Override
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	@Override
	public boolean isModified() {
		if(removed.size() > 0 || edited == true){
			return true;
		}
		return false;
	}
	
	
	
	public boolean isLoadDateImmediate() {
		return loadDateImmediate;
	}

	public void setLoadDateImmediate(boolean loadDateImmediate) {
		this.loadDateImmediate = loadDateImmediate;
	}

	@Override
	public boolean isInvalidCommitted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setInvalidCommitted(boolean isCommitted) {
		// TODO Auto-generated method stub
	}

	@Override
	public void discard() throws SourceException {
		for(Field f :fields){
			f.discard();
		}
	}


	@Override
	public void addValidator(Validator validator) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeValidator(Validator validator) {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<Validator> getValidators() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isInvalidAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setInvalidAllowed(boolean invalidValueAllowed)
			throws UnsupportedOperationException {
		// TODO Auto-generated method stub

	}


	@Override
	public Class<?> getType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addListener(ValueChangeListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeListener(ValueChangeListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public Property getPropertyDataSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getTabIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setTabIndex(int tabIndex) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isRequired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setRequired(boolean required) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setRequiredError(String requiredMessage) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getRequiredError() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void focus() {
		super.focus();
	}

	@Override
	public void setBuffered(boolean buffered) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isBuffered() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeAllValidators() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setValue(Object newValue) throws ReadOnlyException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addValueChangeListener(ValueChangeListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeValueChangeListener(ValueChangeListener listener) {
		// TODO Auto-generated method stub
		
	}




}
