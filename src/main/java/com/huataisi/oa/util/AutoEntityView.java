package com.huataisi.oa.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.transaction.annotation.Transactional;
import org.vaadin.dialogs.ConfirmDialog;

import com.huataisi.oa.domain.User;
import com.huataisi.oa.security.UserUtil;
import com.huataisi.oa.web.EntityEditor;
import com.vaadin.Application;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Runo;

/**
 * Base class for that defines the common layout and some UI logic for all
 * entity views. This class is not specific to any entity class.
 */
@SuppressWarnings("serial")
@Configurable
public class AutoEntityView<E extends IdEntity<E>> extends CustomComponent implements View {
	Logger logger = LoggerFactory.getLogger(AutoEntityView.class);
	
	private VerticalSplitPanel mainLayout;
	private Table table;
	private EntityEditor form;
	private Navigator navigator;
	private boolean dirty = false;

	private Button addButton;
	private CheckBox subSetCheckBox;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private TaskService taskService;

	private int navigateAreaWidth = 150;//单位px
	private int formHeight = 300;//单位px

	private Window mainWindow = (Window) SecurityUtils.getSubject().getSession().getAttribute("mainWindow");

	private Class<E> entityClass;
	
	private JPAContainer<E> tableContainer;
	/**
	 * Constructor for an abstract entity view.
	 * 
	 * The methods {@link #createTable()} and {@link #createForm()} are used to create the main parts of the view.
	 */
	public AutoEntityView(Class<E> entityClass) {
		this.entityClass = entityClass;
		this.tableContainer = ContainerUtils.createJPAContainer(entityClass);
		config();

		//如果子类设置了导航区，增加
		Component naviComp = setNaviComponent();
		if(naviComp != null){
			HorizontalSplitPanel hsPanel = new HorizontalSplitPanel();
			hsPanel.setFirstComponent(naviComp);
			hsPanel.setSecondComponent(mainLayout);
			hsPanel.setSplitPosition(navigateAreaWidth, UNITS_PIXELS);
			setCompositionRoot(hsPanel);
		}else{
			setCompositionRoot(mainLayout);
		}
	}
	
	/**
	 * 设置导航区宽度 px
	 * @param width
	 */
	public void setNavigateAreaWith(int width){
		this.navigateAreaWidth = width;
	}
	public void setFormHeight(int height){
		this.formHeight = height;
	}
	/**
	 * 设置导航区域组件，如果需要请覆盖该方法以设置导航区域。
	 * @return
	 */
	protected Component setNaviComponent(){
		return null;
	}
	private void config(){
		// custom component size, must be set to allow inner layout take 100% size
		setSizeFull();

		mainLayout = new VerticalSplitPanel();
		mainLayout.addStyleName("blue-bottom");
		mainLayout.setSizeFull();

		//添加按钮区
		VerticalLayout buttonsTableArea = new VerticalLayout();
		buttonsTableArea.setSizeFull();
		HorizontalLayout buttonsArea = new HorizontalLayout();
		buttonsArea.setWidth("100%");
		//buttonsArea.setSpacing(true);
		buttonsArea.addComponent(getAddButton());
		configureActions(buttonsArea);
		buttonsArea.addComponent(getSubsetButton());//
		buttonsArea.setComponentAlignment(subSetCheckBox, Alignment.MIDDLE_RIGHT);
		buttonsTableArea.addComponent(buttonsArea);

		//table
		getTable().setImmediate(true);
		getTable().setSelectable(true);
		getTable().setSizeFull();
		getTable().setContainerDataSource(tableContainer);
		//设置标题与可见列
		List<Object> visibleColumns = new ArrayList<Object>();
		for(java.lang.reflect.Field field : entityClass.getDeclaredFields()){
			Caption caption = field.getAnnotation(Caption.class);
			if(caption != null){
				getTable().setColumnHeader(field.getName(), caption.value());
				visibleColumns.add(field.getName());
			}
		}
		getTable().setVisibleColumns(visibleColumns.toArray());
		configureTable(getTable());
		
		buttonsTableArea.addComponent(getTable());
		buttonsTableArea.setExpandRatio(getTable(), 1);
		mainLayout.setFirstComponent(buttonsTableArea);

		//添加form
		mainLayout.setSecondComponent(getForm());
		this.setFormVisible(false);

		// hide buttons if certain operations are not allowed
		getForm().setSaveAllowed(isCreateAllowed() || isUpdateAllowed());
		getForm().setDeleteAllowed(isDeleteAllowed());

		// add listeners for the buttons
		addListeners();

		// initially nothing on the form
		setCurrentEntity(null);
	}

	private CheckBox getSubsetButton() {
		subSetCheckBox = new CheckBox("显示子集"); 
		subSetCheckBox.setImmediate(true);
		subSetCheckBox.setValue(true);

		subSetCheckBox.addListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				setFormVisible(subSetCheckBox.booleanValue());
			}
		});
		return subSetCheckBox;
	}
	private Component getAddButton() {
		// addButton
		addButton = new Button();
		addButton.setStyleName("primary");
		addButton.setCaption("新增");
		addButton.setImmediate(true);
		addButton.setWidth("-1px");
		addButton.setHeight("-1px");
		return addButton;
	}
	// View interface and related

	public void init(Navigator navigator, Application application) {
		this.navigator = navigator;
	}

	@Transactional(readOnly=true)
	public void navigateTo(String requestedDataId) {
		setDirty(false);

		if (null == requestedDataId || "".equals(requestedDataId)) {
			getTable().setValue(null);
			// continue
		} else if (isCreateAllowed() && "new".equals(requestedDataId)) {
			createNewEntity();
			return;
		} else if (isUpdateAllowed() && requestedDataId.startsWith("edit/")) {
			try {
				Object id = requestedDataId.substring(5);
				setCurrentEntity(getEntityForItem(getTable().getItem(id)));
				getTable().setValue(id);
				return;
			} catch (NumberFormatException e) {
				navigateToFragment(null);
				return;
			}
		}else if("search".equals(requestedDataId)){
			doSearch();
			return;
		}

		setCurrentEntity(null);
	}

	public void doSearch() {
	}
	public String getWarningForNavigatingFrom() {
		return (isDirty() && getForm().isModified()) ? "要放弃未保存的修改吗?" : null;
	}

	protected void navigateToFragment(String fragment) {
		if (navigator != null) {
			//TODO 注释很多东西，可能有BUG
//			String uri = navigator.getUri(getClass());
//			// remove fragment
//			uri = uri.replaceAll("/.*", "");
//			if (fragment == null) {
//				fragment = "";
//			} else {
//				fragment = "/" + fragment;
//			}
//			navigator.navigateTo(uri + fragment);
			navigator.navigateTo(fragment);
		}
	}

	protected void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	protected boolean isDirty() {
		return dirty;
	}

	// other methods

	public void createNewEntity() {
		if (isCreateAllowed()) {
			getTable().setValue(null);
			getForm().setDeleteAllowed(false);
			getForm().setSaveAllowed(isCreateAllowed());
			setCurrentEntity(createEntityInstance());

			getForm().focus();
		}
	}

	/**
	 * Adds listeners for the various buttons on the form and for table
	 * selection change.
	 */
	protected void addListeners() {
		getTable().addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				Object value = event.getProperty().getValue();
				if (value != null) {
					navigateToFragment("edit/"+value);
				}
			}
		});

		getForm().addSaveActionListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				Object id = doCommit();
				if (id != null) {
					setDirty(false);
					Notification.show("保存成功");
					//重新设置新值主要是设置version字段，防止hibernate报乐观锁错误
					setCurrentEntity(getEntityForItem(getTable().getItem(id)));
				}
			}
		});
		getForm().addDeleteActionListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				ConfirmDialog.show(mainWindow, "警告:", "删除后将不能恢复，您确定要删除吗？",
						"确定", "取消", new ConfirmDialog.Listener() {
					public void onClose(ConfirmDialog dialog) {
						if (dialog.isConfirmed()) {
							doDelete();
							setDirty(false);
							navigateToFragment(null);
						}
					}
				});

			}
		});
		getForm().addCancelActionListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				setDirty(false);
				navigateToFragment(null);
				//刷新流程
				if(getForm().getProcessDefinitionKey() != null ){
					tableContainer.refresh();
				}
			}
		});

		addButton.addListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				subSetCheckBox.setValue(true);
				createNewEntity();
			}
		});
	}

	/**
	 * Set the current entity to display on the form and to edit.
	 * 
	 * If the table contains an item with the entity as its key, that item is
	 * reused. Otherwise, a new Item is created.
	 * 设置form上要显示的E,用于显示和编辑。
	 * 何时调用 ：
	 * 1.新建时
	 * 2.编辑时
	 * 3.通过url直接访问该应用，但url不是new也不是edit/*时。
	 * 
	 * @param entity
	 */
	private void setCurrentEntity(E entity) {
		if (entity != null) {
			getForm().refresh();

			boolean newEntity = isNewEntity(entity);
			getForm().setDeleteAllowed(isDeleteAllowed());
			boolean saveAllowed = newEntity ? isCreateAllowed() : isUpdateAllowed();
			getForm().setSaveAllowed(saveAllowed);
			setDirty(entity != null && saveAllowed);

			Item item = getItemForEntity(entity);

			getForm().setItemDataSource(item);

			setFormVisible(subSetCheckBox.booleanValue());
		} else {
			getForm().setItemDataSource(null);
			setFormVisible(false);
		}
	}
	private void setFormVisible(boolean show){
		//控制是否显示
		if(show){
			if(mainLayout.getSplitPosition() == 0){
				mainLayout.setSplitPosition(this.formHeight, Layout.UNITS_PIXELS,true);
			}
		}else{
			mainLayout.setSplitPosition(0, Layout.UNITS_PIXELS,true);
		}
	}

	// getters for the components
	private Table getTable() {
		if (table == null) {
			table = createTable();
			table.setStyleName(Runo.TABLE_SMALL);
			table.setPageLength(50);
			table.setNullSelectionAllowed(false);
			table.setRowHeaderMode(Table.ROW_HEADER_MODE_INDEX);
			table.setColumnWidth(null, 24);//设置序号列宽度
		}
		return table;
	}

	private EntityEditor getForm() {
		if (form == null) {
			form = createForm();
			configForm((AutoEntityForm<E>)form);
		}
		return form;
	}

	/**
	 * Creates the table listing the instances of this entity.
	 * 
	 * Subclasses can override this.
	 * 
	 * @return
	 */
	protected Table createTable() {
		final Table table = new Table() {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			@Override
			protected String formatPropertyValue(Object rowId,
					Object colId, Property property) {
				// Format by property type
				if (property.getType() == Date.class) {
					Date date = (Date) property.getValue();
					String sv = date==null?"":sdf.format(date);
					return sv;
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		return table;
	}

	/**
	 * Refresh the table and any other relevant parts of the entity view.
	 * 
	 * This is called after entities are saved or deleted or when navigating to a new view.
	 */
	protected void refresh() {
		Object sortContainerPropertyId = getTable()
				.getSortContainerPropertyId();
		boolean sortAscending = getTable().isSortAscending();

		//		configureTable(getTable());

		if (sortContainerPropertyId != null) {
			getTable().setSortAscending(sortAscending);
			getTable().setSortContainerPropertyId(sortContainerPropertyId);
			getTable().sort();
		}
	}

	
	private E createEntityInstance(){
		try {
			return entityClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// these are typically defined in the concrete subclass

	/**
	 * Create the form used for editing an entity.
	 * 
	 * Concrete subclasses must implement this method.
	 * 
	 * @return
	 */
	protected  EntityEditor createForm(){
		AutoEntityForm<E> form = new AutoEntityForm<E>(entityClass);
		return form;
	}

	protected void configureTable(Table table){
		
	}
	protected void configureActions(HorizontalLayout actionArea) {
	}

	@Transactional
	public Object doCommit() {
		E entity = getEntityForItem(getForm().getItemDataSource());
		Object id = getIdForEntity(entity);
		String pdKey = getForm().getProcessDefinitionKey();
		try {
			getForm().commit();
			if (id == null ) {
				id = tableContainer.addEntity(entity);//添加实体，同返回ID
				entity = tableContainer.getItem(id).getEntity();//获得保存过的实体，用于存入工作流

				//新建的，启动工作流 
				if(pdKey != null){
					Map<String,Object> var = new HashMap<String,Object>();
					var.put("order", entity);
					runtimeService.startProcessInstanceByKey(pdKey,id.toString(), var);
					Task task = taskService.createTaskQuery().processInstanceBusinessKey(id.toString()).singleResult();
					taskService.complete(task.getId());
					tableContainer.refresh();
				}
			}else if(id != null && pdKey != null){
				//非新建的(一般是退回的)，完成工作流
				User loginUser = UserUtil.getCurrentUser();
				Task task = taskService.createTaskQuery()
						.processInstanceBusinessKey(id.toString())
						.taskAssignee(loginUser.getUsername())
						.singleResult();
				if(task != null){
					taskService.complete(task.getId());
					tableContainer.refresh();
				}
			}

			return id;
		} catch (InvalidValueException e) {
			// show validation error also on the save button
			getForm().setCommitErrorMessage(e.getMessage());
			return null;
		}catch (Exception e) {
			if(e.getMessage()!= null && e.getMessage().indexOf("Duplicate") != -1){
				getForm().setCommitErrorMessage("记录重复"+e.getMessage());
			}else{
				getForm().setCommitErrorMessage("保存出错:"+e.getMessage());
			}
			logger.error("保存出错", e);
			throw new RuntimeException(e);//只有抛出异常才会回退事务
		}
	}

	@Transactional
	public void doDelete() {

		Object id = getIdForEntity(getEntityForItem(getForm().getItemDataSource()));
		if (id != null) {
			try {
				tableContainer.removeItem(id);
			} catch (javax.persistence.PersistenceException e) {
				Notification.show("删除出错，该记录可能被引用，禁止删除！"+e.getMessage());
			}
			catch (Exception e) {
				e.printStackTrace();
				Notification.show("删除出错。"+e.getMessage());
			}
		}
	}
	
    public boolean isCreateAllowed() {
        return true;
    }
    
    public boolean isUpdateAllowed() {
        return true;
    }
    
    public boolean isDeleteAllowed() {
        return true;
    }
    public String getIdProperty() {
        return "id";
    }
    
    public String getVersionProperty() {
        return "version";
    }
    
	public E saveEntity(E entity) {
        if (entity == null) {
            return null;
        }
        return (E) entity.persist();
    }
    public void deleteEntity(E entity) {
        if (entity != null) {
            entity.remove();
        }
    }
    
    
    public boolean isNewEntity(E entity) {
        return (entity != null && entity.getId() == null);
    }
    
    /**
	 * Returns the item identifier for an entity instance.
	 * 
	 * @param entity
	 *            the entity for which to get the identifier
	 * @return Object item identifier
	 */
    private Object getIdForEntity(E entity) {
        return entity != null ? entity.getId() : null;
    }
  
    /**
 	 * Returns an existing or new Item for an entity to be edited on a form. For
 	 * already stored entities, the item is retrieved from the table. For new
 	 * entities, a new item is created.
 	 * 
 	 * @param entity
 	 * @return Item
 	 */
	private Item getItemForEntity(E entity) {
        Item item = getTable().getItem(entity.getId());
        if (item == null) {
            item = tableContainer.createEntityItem(entity);
        }
        return item;
    }
    
    /**
	 * Obtains the entity from an item in a container specific manner.
	 * 
	 * @param item
	 *            the item for which to get the entity
	 * @return entity
	 */
    private E getEntityForItem(Item item) {
        if (item instanceof EntityItem) {
            return((EntityItem<E>) item).getEntity();
        } else {
            return null;
        }
    }
    
    public JPAContainer<E> getTableContainer(){
    	return tableContainer;
    }
    
    public void configForm(AutoEntityForm<E> entityForm){
    	
    }

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}
}
