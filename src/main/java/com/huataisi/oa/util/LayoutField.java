package com.huataisi.oa.util;

import com.vaadin.ui.Field;

/**
 * 可设置自己占多少个单元格的field,fieldName不允许null,field可以为null.<br/>
 * 可以调用其equals方法与String相比，当fieldName与String equals 结果为true时,该结果也为true.
 * @author Administrator
 */
public class LayoutField{
	private static final String errorMsg = "fieldName不能为null";
	private String fieldName;
	private Field field;
	private int colSpan = 1;
	private int rowSpan = 1;
	/**
	 * @param fieldName 字段名，为null时将抛出RuntimeException
	 * @param colSpan 占多少列，必须是大于0的整数，小于或等于0的将被重置为1,默认值1
	 * @param rowSpan 占多少行，必须是大于0的整数，小于或等于0的将被重置为1,默认值1
	 */
	public LayoutField(String fieldName, int colSpan, int rowSpan) {
		if(fieldName == null) throw new RuntimeException(errorMsg);
		this.fieldName = fieldName;
		this.colSpan = colSpan > 0 ? colSpan : 1;
		this.rowSpan = rowSpan > 0 ? rowSpan : 1;
	}
	/**
	 * @param fieldName 字段名，为null时将抛出RuntimeException
	 * @param field 如果为null系统会自动生成如果不为null,系统则使用该field
	 * @param colSpan 占多少列，必须是大于0的整数，小于或等于0的将被重置为1,默认值1
	 * @param rowSpan 占多少行，必须是大于0的整数，小于或等于0的将被重置为1,默认值1
	 */
	public LayoutField(String fieldName, Field field, int colSpan,int rowSpan) {
		if(fieldName == null) throw new RuntimeException(errorMsg);
		this.fieldName = fieldName;
		this.field = field;
		this.colSpan = colSpan > 0 ? colSpan : 1;
		this.rowSpan = rowSpan > 0 ? rowSpan : 1;
	}

	/**
	 * @param fieldName 字段名，为null时将抛出RuntimeException
	 * @param field 如果为null系统会自动生成如果不为null,系统则使用该field
	 */
	public LayoutField(String fieldName, Field field) {
		if(fieldName == null) throw new RuntimeException(errorMsg);
		this.fieldName = fieldName;
		this.field = field;
	}

	@Override
	public String toString(){
		return this.fieldName;
	}
	@Override
	public int hashCode() {
		return fieldName.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (obj instanceof String){
			if(obj.equals(fieldName)){
				return true;
			}
		}
		if (getClass() != obj.getClass())
			return false;
		LayoutField other = (LayoutField) obj;
		if (fieldName == null) {
			if (other.fieldName != null)
				return false;
		} else if (!fieldName.equals(other.fieldName))
			return false;
		return true;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public Field getField() {
		return field;
	}
	public void setField(Field field) {
		this.field = field;
	}
	public int getColSpan() {
		return colSpan;
	}
	public void setColSpan(int colSpan) {
		this.colSpan = colSpan;
	}
	public int getRowSpan() {
		return rowSpan;
	}
	public void setRowSpan(int rowSpan) {
		this.rowSpan = rowSpan;
	}
	
}