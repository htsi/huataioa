package com.huataisi.oa.salary;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.huataisi.oa.domain.Duty;
import com.huataisi.oa.domain.User;
import com.huataisi.oa.security.UserUtil;
import com.huataisi.oa.util.AutoEntityView;
import com.huataisi.oa.web.EntityEditor;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.Property;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.ui.Table;

public class SalaryView extends AutoEntityView<com.huataisi.oa.salary.Salary> {


    public SalaryView() {
		super(Salary.class);
	}
    
	@Override
	protected EntityEditor createForm() {
		return new SalaryFormA();
	}

	@Override
    protected void configureTable(Table table) {
        setFilter();
        table.setVisibleColumns(new Object[]{"salaryUser","salaryDate","baseSalary","performanceSalary"
        		,"mealSalary","senioritySalary","fullAttendanceSalary","computerSubsidySalary",
        		"projectBonusSalary","deductionsAttendanceSalary","homePaySalary"});
 
    }
    /**
     * 设置数据权限
     */
    private void setFilter(){
    	User user = UserUtil.getCurrentUser();
    	
    	JPAContainer<Salary> con = getTableContainer();
    	con.removeAllContainerFilters();
//    	if(Duty.ZHI_YUAN.equals(user.getDuty().getName())){
//    		//职员只能看自己的
//    		Filter filter = new Compare.Equal("salaryUser", user);
//    		con.addContainerFilter(filter);
//    	}else if(Duty.BU_MENG_JING_LI.equals(user.getDuty().getName())
//    			&& "后勤部".equals(user.getDepartment().getName())
//    			){
//    		//后勤部门经理可以看全部
//	    }else if(Duty.BU_MENG_JING_LI.equals(user.getDuty().getName())){
//	    	//部门经理只能查看本部门的
//	    	Set<User> myDepUsers = user.getDepartment().getUsers();
//			List<Equal> eqs = new ArrayList<Equal>();
//			for(User u: myDepUsers){
//				eqs.add(new Equal("salaryUser", u));
//			}
//			Or or = new Or((Equal[]) eqs.toArray(new Equal[eqs.size()]));
//	    	con.addContainerFilter(or);
//	    }
    	
    	if((Duty.BU_MENG_JING_LI.equals(user.getDuty().getName()) && "后勤部".equals(user.getDepartment().getName()))
    		|| Duty.ZONG_JING_LI.equals(user.getDuty().getName())
    			){
    		//后勤部门经理可以看全部
	    }else {
	    	Filter filter = new Compare.Equal("salaryUser", user);
    		con.addContainerFilter(filter);
	    }
    	
    	
    }
	/**
	 * 格式化table的日期格式
	 */
	@Override
	protected Table createTable() {
		final Table table = new Table() {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
			@Override
			protected String formatPropertyValue(Object rowId,
					Object colId, Property property) {
				// Format by property type
				if (property.getType() == Date.class) {
					Date date = (Date) property.getValue();
					String sv = date==null?"":sdf.format(date);
					return sv;
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		return table;
	}

}
