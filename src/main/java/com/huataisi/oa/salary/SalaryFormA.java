package com.huataisi.oa.salary;

import com.huataisi.oa.domain.Duty;
import com.huataisi.oa.domain.User;
import com.huataisi.oa.security.UserUtil;
import com.huataisi.oa.util.AutoEntityForm;
import com.vaadin.data.Item;

public class SalaryFormA extends AutoEntityForm<Salary> {

	public SalaryFormA() {
		super(Salary.class);
	}

	@Override
	public void setItemDataSource(Item item) {
		User user = UserUtil.getCurrentUser();
		
		if((Duty.BU_MENG_JING_LI.equals(user.getDuty().getName())
				&& "后勤部".equals(user.getDepartment().getName()))
				||Duty.ZONG_JING_LI.equals(user.getDuty().getName())
				){
			this.setSaveAllowed(true);
			this.setDeleteAllowed(true);
		}else{
			this.setSaveAllowed(false);
			this.setDeleteAllowed(false);
		}
		super.setItemDataSource(item);
	}
	
	
}
