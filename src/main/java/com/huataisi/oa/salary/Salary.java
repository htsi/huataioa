package com.huataisi.oa.salary;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.huataisi.oa.domain.User;
import com.huataisi.oa.util.Caption;
import com.huataisi.oa.util.IdEntity;

@Entity
@Table(name="OA_GZ_SALARY")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Salary extends IdEntity<Salary>{
	
	@NotNull
	@Caption("\u59D3\u540D")
	@ManyToOne
	private User salaryUser;
	
	@NotNull
	@Caption("月度")
	private Date salaryDate;
	
	@NotNull
	@Caption("\u57FA\u672C\u5DE5\u8D44")
	private double baseSalary;
	
	@NotNull
	@Caption("\u7EE9\u6548\u5DE5\u8D44")
	private double performanceSalary;
	
	@NotNull
	@Caption("\u9910\u8865")
	private double mealSalary;
	
	@NotNull
	@Caption("\u5DE5\u9F84\u5DE5\u8D44")
	private double senioritySalary;
	
	@NotNull
	@Caption("\u5168\u52E4\u5956")
	private double fullAttendanceSalary;
	
	@NotNull
	@Caption("\u7535\u8111\u8865\u52A9")
	private double computerSubsidySalary;
	
	@NotNull
	@Caption("\u9879\u76EE\u5956\u91D1")
	private double projectBonusSalary;
	
	
	@NotNull
	@Caption("\u8003\u52E4\u6263\u6B3E")
	private double deductionsAttendanceSalary;
	
	@NotNull
	@Caption("\u5B9E\u53D1\u5DE5\u8D44")
	private double homePaySalary;
	
	@NotNull
	@Caption("创建人")
	@ManyToOne
	private User createBy;
	
	@NotNull
	@Caption("\u751F\u6210\u65F6\u95F4")
	private Date createDate;
	
	@Caption("\u5907\u6CE8")
	private String remarks;

	public User getSalaryUser() {
		return salaryUser;
	}

	public void setSalaryUser(User salaryUser) {
		this.salaryUser = salaryUser;
	}

	public Date getSalaryDate() {
		return salaryDate;
	}

	public void setSalaryDate(Date salaryDate) {
		this.salaryDate = salaryDate;
	}

	public double getBaseSalary() {
		return baseSalary;
	}

	public void setBaseSalary(double baseSalary) {
		this.baseSalary = baseSalary;
	}

	public double getPerformanceSalary() {
		return performanceSalary;
	}

	public void setPerformanceSalary(double performanceSalary) {
		this.performanceSalary = performanceSalary;
	}

	public double getMealSalary() {
		return mealSalary;
	}

	public void setMealSalary(double mealSalary) {
		this.mealSalary = mealSalary;
	}

	public double getSenioritySalary() {
		return senioritySalary;
	}

	public void setSenioritySalary(double senioritySalary) {
		this.senioritySalary = senioritySalary;
	}

	public double getFullAttendanceSalary() {
		return fullAttendanceSalary;
	}

	public void setFullAttendanceSalary(double fullAttendanceSalary) {
		this.fullAttendanceSalary = fullAttendanceSalary;
	}

	public double getComputerSubsidySalary() {
		return computerSubsidySalary;
	}

	public void setComputerSubsidySalary(double computerSubsidySalary) {
		this.computerSubsidySalary = computerSubsidySalary;
	}

	public double getProjectBonusSalary() {
		return projectBonusSalary;
	}

	public void setProjectBonusSalary(double projectBonusSalary) {
		this.projectBonusSalary = projectBonusSalary;
	}

	public double getDeductionsAttendanceSalary() {
		return deductionsAttendanceSalary;
	}

	public void setDeductionsAttendanceSalary(double deductionsAttendanceSalary) {
		this.deductionsAttendanceSalary = deductionsAttendanceSalary;
	}

	public double getHomePaySalary() {
		return homePaySalary;
	}

	public void setHomePaySalary(double homePaySalary) {
		this.homePaySalary = homePaySalary;
	}

	public User getCreateBy() {
		return createBy;
	}

	public void setCreateBy(User createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
}
